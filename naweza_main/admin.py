import csv
import time
from django.contrib import admin
from django.http import HttpResponse
from .models import SetupList


def dump_to_csv(modeladmin, request, qs):
    """
    These takes in a Django queryset and spits out a CSV file.

    Generic method for any queryset
    """
    model = qs.model
    file_id = 'NAWEZA_%s_%d' % (model.__name__, int(time.time()))
    file_name = 'attachment; filename=%s.csv' % (file_id)
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = file_name
    writer = csv.writer(response, csv.excel)

    headers = []
    for field in model._meta.fields:
        headers.append(field.name)
    writer.writerow(headers)

    for obj in qs:
        row = []
        for field in headers:
            val = getattr(obj, field)
            if callable(val):
                val = val()
            if type(val) == unicode:
                val = val.encode("utf-8")
            row.append(val)
        writer.writerow(row)
    return response

dump_to_csv.short_description = u"Dump to CSV"

# Register your models here.


class GeneralModelAdmin(admin.ModelAdmin):
    """Admin back end for Lookup lists management."""

    search_fields = ['item_id', 'item_description']
    list_display = ['item_id', 'item_description', 'field_name',
                    'item_category', 'item_sub_category', 'the_order',
                    'is_void']
    readonly_fields = ['is_void']
    list_filter = ['field_name']
    actions = [dump_to_csv]


admin.site.register(SetupList, GeneralModelAdmin)
