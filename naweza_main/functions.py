# -*- coding: utf-8 -*-
import collections
from collections import OrderedDict
import simplejson as json
import os
import xlrd
from datetime import datetime
from django.core.cache import cache
from django.db.models import Q, Count
from django.conf import settings
# from openpyxl import load_workbook
from naweza_main.models import SetupGeography, SetupList
from naweza_registry.models import RegOrgUnit, RegPerson, AfyaData


def convert_date(d_string, fmt='%d-%b-%Y'):
    try:
        if isinstance(d_string, datetime.date):
            new_date = datetime.datetime.strptime(d_string, fmt)
        else:
            new_date = datetime.datetime.strptime(d_string, fmt)
    except Exception, e:
        error = 'Error converting date -%s' % (str(e))
        print error
        return d_string
    else:
        return new_date


def decode_algorithm(values, results): 
    condition = None
    SBP = None
    DBP = None
    AGE = None
    CHOLESTROL = None

    for value in values:
        if value:
            value = str(value).split();
            val = value[0]
            suffix = value[1]
            # get SBP/DBP
            if(suffix=='mmHg(SBP)'):
                SBP = int(val)
            if(suffix=='mmHg(DBP)'):
                DBP = int(val)
            if(suffix=='yrs'):
                AGE = int(val)
            if(suffix=='mmol/L'):
                CHOLESTROL = int(val)


            # generate conditions
            print SBP, DBP
            if((SBP >=140 and SBP <180) and DBP >= 90) and 'RLV3' in results:
                condition = '20 - < 30% and BP >= 140/90 mmHg'
                results.append(condition)

            if(SBP >=180 and (DBP >= 30 and DBP <90)) and ('RLV4' or 'RLV5') in results:
                condition = '> 30% and BP >= 180/30 mmHg'
                results.append(condition)

            if((SBP >=130 and SBP < 140) and (DBP >= 80 and DBP < 90)) and 'DGDM' in results:
                condition = 'DGDM and BP >= 130/80 mmHg'
                results.append(condition)

            if(AGE >= 40) and 'DGDM' in results:
                condition = 'DGDM and age >= 40'
                results.append(condition)

            if(SBP >=160 and DBP >= 100):
                condition = 'BP >= 160/100 mmHg'
                results.append(condition)

            if(CHOLESTROL >= 8):
                condition = '>= 8 mmol/L'
                results.append(condition)

            if AGE > 55:
                condition = 'age > 55 yrs'
                results.append(condition)

            if AGE < 55:
                condition = 'age < 55 yrs'
                results.append(condition)

            # append generated condtions if not exists
            #print 'condition = %s' %condition
            #if condition not in results:
            #    results.append(condition)
    return results

def get_drugs(conditions):
        exists = False
        jsonData = []
        jsonArray = get_algorithm(conditions)
        drug_actions = ['Consider drugs(Antihypertensive medications)']
       
        for jsonString in jsonArray:
            action = jsonString['action']
            if action in drug_actions:
                exists = True;
       

        if exists:
            file_path = os.path.join(settings.DOCUMENT_ROOT, 'naweza_drugs.xlsx')

            # Open the workbook and select the first worksheet
            wb = xlrd.open_workbook(file_path)
            sh = wb.sheet_by_index(0)
            # actions = OrderedDict()

            actions = []
            values = conditions['values']
            results = conditions['results']
            decoded_values = decode_algorithm(values, results);

            for result in decoded_values:
                # Iterate through each row in worksheet and fetch values into dict
                for rownum in range(1, sh.nrows):
                    row_values = sh.row_values(rownum)
                    if row_values[1] != '#':
                        action = row_values[4]
                        if result == row_values[3]:
                            if action not in actions:
                                actions.append(action)
             # Create JSON
            for action in actions:
                jsonData.append({'action': str(action)})           
        else:
            jsonData.append({'action': 'No antihypertensive medication tips'})

        return jsonData


def get_algorithm(conditions):
    file_path = os.path.join(settings.DOCUMENT_ROOT, 'naweza_algorithm.xlsx')

    # Open the workbook and select the first worksheet
    wb = xlrd.open_workbook(file_path)
    sh = wb.sheet_by_index(0)
    # actions = OrderedDict()

    jsonData = []
    actions = []
    values = conditions['values']
    results = conditions['results']
    decoded_values = decode_algorithm(values, results);

    for result in decoded_values:
        # Iterate through each row in worksheet and fetch values into dict
        for rownum in range(1, sh.nrows):
            row_values = sh.row_values(rownum)
            if row_values[1] != '#':
                action = row_values[4]
                ## if(action=='Consider drugs(Antihypertensive medications)'):
                if result == row_values[3]:
                    if action not in actions:
                        actions.append(action)
    # Create JSON
    if actions:
        for action in actions:
            jsonData.append({'action': str(action)})
    else:
        jsonData.append({'action': 'No patient management tips'})

    return jsonData

def get_general_list(field_names=[], item_category=False):
    '''
    Get list general filtered by field_name
    '''
    try:
        queryset = SetupList.objects.filter(is_void=False).order_by(
            'id', 'the_order')
        if len(field_names) > 1:
            q_filter = Q()
            for field_name in field_names:
                q_filter |= Q(**{"field_name": field_name})
            queryset = queryset.filter(q_filter)
        else:
            queryset = queryset.filter(field_name=field_names[0])
        if item_category:
            queryset = queryset.filter(item_category=item_category)
    except Exception, e:
        error = 'Error getting whole list - %s' % (str(e))
        print error
        return None
    else:
        return queryset


def get_list(field_name=[], default_txt=False, category=False):
    my_list = ()
    try:
        cat_id = '1' if category else '0'
        cache_key = 'set_up_list_%s_%s' % (field_name, cat_id)
        cache_list = cache.get(cache_key)
        if cache_list:
            v_list = cache_list
            print 'FROM Cache %s' % (cache_key)
        else:
            v_list = get_general_list([field_name], category)
            cache.set(cache_key, v_list, 300)
        my_list = v_list.values_list('item_id', 'item_description')
        if default_txt:
            initial_list = ('', default_txt)
            final_list = [initial_list] + list(my_list)
            return final_list
    except Exception, e:
        error = 'Error getting list - %s' % (str(e))
        print error
        return my_list
    else:
        return my_list


def get_dict(field_name=[], default_txt=False):
    '''
    Push the item_id and item_description into a tuple
    Instead of sorting after, ordered dict works since query
    results are already ordered from db
    '''
    # initial_list = {'': default_txt} if default_txt else {}
    # all_list = collections.OrderedDict(initial_list)
    # [{'item_id': u'TNRS', 'item_description': u'Residentia....'}
    dict_val = {}
    try:
        my_list = get_general_list(field_names=field_name)
        all_list = my_list.values('item_id', 'item_description')
        for value in all_list:
            item_id = value['item_id']
            item_details = value['item_description']
            dict_val[item_id] = item_details
    except Exception, e:
        error = 'Error getting list - %s' % (str(e))
        print error
        return {}
    else:
        return dict_val


def get_org_units_list(default_txt=False, org_types=[]):
    '''
     Get all org_unit_name + org_unit__id
    '''
    initial_list = {'': default_txt} if default_txt else {}
    all_list = collections.OrderedDict(initial_list)
    try:
        my_list = RegOrgUnit.objects.filter(
            is_void=False).order_by('org_unit_name')
        for a_list in my_list:
            unit_names = '%s' % (a_list.org_unit_name)
            unit_type = str(a_list.org_unit_type)
            if org_types:
                if unit_type in org_types:
                    all_list[a_list.org_unit] = unit_names
            else:
                all_list[a_list.org_unit] = unit_names
    except Exception, e:
        error = 'Error getting list - %s' % (str(e))
        print error
        return ()
    else:
        return all_list.items


def tokenize_search_string(search_string):
    if not search_string:
        return []
    return search_string.split()


def get_list_of_persons(search_string, search_criteria):
    # print 'get_list_of_persons(search_string %s, search_criteria %s)' %(search_string, search_criteria)
    search_strings = tokenize_search_string(search_string)
    q_filter = Q() 

    # Search user
    if search_criteria == 'PTUS':
        field_names = ['person','first_name', 'other_names', 'surname']
        for field in field_names:
            q_filter |= Q(**{"%s__icontains" % field: search_string})
        queryset = RegPerson.objects.filter(q_filter)
        # queryset = queryset.filter(designation=search_criteria)
    else:
        # Search patient
        # if search_criteria == 'PTPT':
        field_names = ['id','fname', 'lname']
        for field in field_names:
            q_filter |= Q(**{"%s__icontains" % field: search_string})
        queryset = AfyaData.objects.filter(q_filter)
        # queryset = queryset.filter(designation=search_criteria)
    return queryset


def get_list_of_orgs(search_string):
    search_strings = tokenize_search_string(search_string)
    if search_strings:
        q_filter = Q()
        field_names = ['org_unit_name']
        for field in field_names:
            q_filter |= Q(**{"%s__icontains" % field: search_string})
        queryset = RegOrgUnit.objects.filter(q_filter)
    else:
        queryset = RegOrgUnit.objects.all()
    return queryset


def convert_date(d_string, fmt='%d-%b-%Y'):
    try:
        d_string = datetime.strptime(d_string, fmt)
        if isinstance(d_string, datetime.datetime):
            new_date = datetime.strptime(d_string, fmt)
        else:
            new_date = datetime.strptime(d_string, fmt)
    except Exception, e:
        error = 'Error converting date -%s' % (str(e))
        print error
        return d_string
    else:
        return new_date.date()

def translate(value):
    if value:
        item_value = SetupList.objects.filter(item_id=value, is_void=False)
        if item_value.count() > 0:
            item_value = item_value[0]
            return item_value.item_description
    else:
        return value
    return value

