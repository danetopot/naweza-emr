# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='SetupAlgorithm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('variable', models.CharField(max_length=255)),
                ('source', models.CharField(max_length=255)),
                ('condition', models.CharField(max_length=255, null=True)),
                ('action', models.CharField(max_length=1000, null=True, blank=True)),
                ('is_void', models.BooleanField(default=False)),
                ('timestamp_modified', models.DateTimeField(default=django.utils.timezone.now)),
            ],
            options={
                'db_table': 'naweza_algorithm',
            },
        ),
        migrations.CreateModel(
            name='SetupGeography',
            fields=[
                ('area_id', models.IntegerField(serialize=False, primary_key=True)),
                ('area_type_id', models.CharField(max_length=50)),
                ('area_name', models.CharField(max_length=100)),
                ('area_code', models.CharField(max_length=10, null=True)),
                ('parent_area_id', models.IntegerField(null=True)),
                ('area_name_abbr', models.CharField(max_length=5, null=True)),
                ('timestamp_created', models.DateTimeField(default=django.utils.timezone.now)),
                ('timestamp_updated', models.DateTimeField(default=django.utils.timezone.now)),
                ('is_void', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'list_geo',
                'verbose_name': 'Setup Geography',
                'verbose_name_plural': 'Setup Geographies',
            },
        ),
        migrations.CreateModel(
            name='SetupList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('item_id', models.CharField(max_length=4)),
                ('item_description', models.CharField(max_length=255)),
                ('item_description_short', models.CharField(max_length=26, null=True)),
                ('item_category', models.CharField(max_length=255, null=True, blank=True)),
                ('item_sub_category', models.CharField(max_length=255, null=True, blank=True)),
                ('the_order', models.IntegerField(null=True)),
                ('user_configurable', models.BooleanField(default=False)),
                ('sms_keyword', models.BooleanField(default=False)),
                ('is_void', models.BooleanField(default=False)),
                ('field_name', models.CharField(max_length=200, null=True, blank=True)),
                ('timestamp_modified', models.DateTimeField(default=django.utils.timezone.now)),
            ],
            options={
                'db_table': 'list_general',
            },
        ),
    ]
