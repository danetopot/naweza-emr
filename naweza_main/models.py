# Create your models here.
import base64
from django.db import models
from django.utils import timezone
import uuid


class SetupGeography(models.Model):
    """List of Geographical areas of Kenya."""

    area_id = models.IntegerField(primary_key=True)
    area_type_id = models.CharField(max_length=50)
    area_name = models.CharField(max_length=100)
    area_code = models.CharField(max_length=10, null=True)
    parent_area_id = models.IntegerField(null=True)
    area_name_abbr = models.CharField(max_length=5, null=True)
    timestamp_created = models.DateTimeField(default=timezone.now)
    timestamp_updated = models.DateTimeField(default=timezone.now)
    is_void = models.BooleanField(default=False)

    class Meta:
        """Override some params."""

        db_table = 'list_geo'
        verbose_name = 'Setup Geography'
        verbose_name_plural = 'Setup Geographies'


class SetupList(models.Model):
    """List used for drop downs and other selections."""

    item_id = models.CharField(max_length=4)
    item_description = models.CharField(max_length=255)
    item_description_short = models.CharField(max_length=26, null=True)
    item_category = models.CharField(max_length=255, null=True, blank=True)
    item_sub_category = models.CharField(max_length=255, null=True, blank=True)
    the_order = models.IntegerField(null=True)
    user_configurable = models.BooleanField(default=False)
    sms_keyword = models.BooleanField(default=False)
    is_void = models.BooleanField(default=False)
    field_name = models.CharField(max_length=200, null=True, blank=True)
    timestamp_modified = models.DateTimeField(default=timezone.now)

    class Meta:
        """Override some params."""

        db_table = 'list_general'


class SetupAlgorithm(models.Model):
    """List used for drop downs and other selections."""
    variable = models.CharField(max_length=255)
    source = models.CharField(max_length=255)
    condition = models.CharField(max_length=255, null=True)
    action = models.CharField(max_length=1000, null=True, blank=True)
    is_void = models.BooleanField(default=False)
    timestamp_modified = models.DateTimeField(default=timezone.now)

    class Meta:
        """Override some params."""

        db_table = 'naweza_algorithm'
