from __future__ import absolute_import

from celery import Celery

# app = Celery('tasks', broker='amqp://guest@localhost//')
app = Celery('tasks', backend='rpc://', broker='pyamqp://')

@app.task
def test():
	print '*******RabbitMQ Testing ********'
	return