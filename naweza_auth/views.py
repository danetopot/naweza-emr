from django.core.urlresolvers import reverse
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.contrib import messages
from django.utils import timezone
from django.core import serializers
from django.conf import settings
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required
import uuid
from naweza_registry.models import RegPerson, RegOrgUnit, RegPersonsOrgUnits
from naweza_auth.models import AppUser
from naweza.views import home as naweza_home
from naweza_auth.forms import LoginForm, PasswordResetForm, RegistrationForm

# Create your views here.
def home(request):
    """Some default page for the home page / Dashboard."""
    try:
        return render(request, 'base.html')
    except Exception, e:
        raise e

def login(request):
    """Method to handle log in to system."""
    try:            
        form = LoginForm()
        if request.method == 'POST':
            username = request.POST['username']
            password = request.POST['password']

            from django.contrib.auth import get_user_model
            user = get_user_model()
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    auth_login(request, user)
                    user_names = '%s %s' % (user.first_name, user.last_name)
                    request.session['names'] = user_names
                    #print 'log in Naweza EMR user (%s)' %(username)           
                    return HttpResponseRedirect(reverse(naweza_home))
                else:
                    msg = "Login Account is currently inactive."
                    messages.add_message(request, messages.ERROR, msg)
                    return render(request, 'login.html', {'form': form})
            else:
                msg = "Incorrect username and / or password."
                messages.add_message(request, messages.ERROR, msg)
                return render(request, 'login.html', {'form': form})
        return render(request, 'login.html', { 'form': form })
    except Exception, e:
        print 'Error login - %s' % (str(e))
        raise e

def logout(request):
    auth_logout(request)
    return HttpResponseRedirect(reverse(login))

def access(request):
    """Some default page for access login."""
    try:
        if request.method == 'POST':
            print 'creating new user'

            first_name = request.POST.get('first_name')
            surname = request.POST.get('surname')
            sex_id = request.POST.get('sex_id')
            email = request.POST.get('email')
            clinic = request.POST.get('clinic')
            username = request.POST.get('username')
            password1 = request.POST.get('password1')
            password2 = request.POST.get('password2')


            # resolve passwords
            if password1 == password2:
                password = password1
            else:
                msg = 'Passwords do not match!'
                messages.add_message(request, messages.ERROR, msg)
                form = RegistrationForm(data=request.POST)
                return render(request, 'home.html', {'status': 200, 'form': form })

            # validate username if__exists
            username_exists = AppUser.objects.filter(username=username)
            if username_exists:
                msg = 'Username (%s) is taken. Pick another one.' % username
                messages.add_message(request, messages.ERROR, msg)
                form = RegistrationForm(data=request.POST)
                return render(request, 'home.html', {'status': 200, 'form': form })
            else:
                # Create Person
                person = RegPerson(
                    designation = 'PTUS', first_name = first_name, other_names = None,
                    surname = surname, email = email, des_phone_number = None,
                    date_of_birth = None, date_of_death = None,
                    sex_id = sex_id,
                    is_void = False, created_by =None, rec_status = 'I'
                    )
                person.save()

                reg_person_pk = person.pk
                print 'person(%s), clinic(%s)' %(str(reg_person_pk), str(clinic))
                now = timezone.now()
                # Attach Person to Clinic
                RegPersonsOrgUnits(
                        person=RegPerson.objects.get(pk=str(reg_person_pk)),
                        org_unit=RegOrgUnit.objects.get(pk=str(clinic)),
                        date_linked=now,
                        date_delinked=None,
                        is_void=False).save()

                # Create User
                user = AppUser.objects.create_user(username=username,
                                                   reg_person=str(reg_person_pk),
                                                   is_active=False,
                                                   password=password)
                msg = 'User (%s) created successfully.' % (username)
                messages.add_message(request, messages.INFO, msg)
                return HttpResponseRedirect(reverse(login))
                # return HttpResponseRedirect('%s?id=%d' % (reverse(login), int(person_id)))
        form = RegistrationForm()
        return render(request, 'home.html', {'status': 200, 'form': form })
    except Exception, e:
        raise e

def activate_user(request):
    jsonData = [] 
    if request.method == 'POST':
        current_user = request.user
        if current_user.id == 1:
            person = request.POST.get('person') 
            op = int(request.POST.get('op'))
            if op == 1:
                try:
                    appuser = AppUser.objects.get(reg_person_id=person) 
                    appuser.is_active = True           
                    appuser.save(update_fields=['is_active'])
                    jsonData.append({ 
                        'msg_type': 'Success',
                        'msg_text': 'User activated!' })
                except Exception as e:
                    jsonData.append({ 
                        'msg_type': 'Error',
                        'msg_type': 'User activation error - ' + str(e) + '!' })
                    raise e
            if op == 2:
                try:
                    appuser = AppUser.objects.get(reg_person_id=person) 
                    appuser.is_active = False           
                    appuser.save(update_fields=['is_active'])
                    jsonData.append({ 
                        'msg_type': 'Success',
                        'msg_text': 'User deactivated!' })
                except Exception as e:
                    jsonData.append({ 
                        'msg_type': 'Error',
                        'msg_type': 'User deactivation error - ' + str(e) + '!' })
                    raise e
        else:
            jsonData.append({ 
                'msg': 'Permissions',
                'msg_type': 'You have insufficient permissions to perform this operation!' })
    return JsonResponse(jsonData, content_type='application/json', safe=False)

def password_reset(request):
    """Method to handle log in to system."""
    try:
        if request.method == 'POST':
            print 'reset password for user in Naweza EMR . . .'
            return HttpResponseRedirect(reverse(login))
        form = PasswordResetForm()
        return render(request, 'authentication\password_reset.html', { 'form': form })
    except Exception, e:
        print 'Error resetting password - %s' % (str(e))
        raise e
