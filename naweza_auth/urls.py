"""URLs for authentication module."""
from django.conf.urls import patterns, url

# This should contain urls related to auth app ONLY
urlpatterns = patterns('naweza_auth.views',
                       url(r'^$', 'home')
                       )
