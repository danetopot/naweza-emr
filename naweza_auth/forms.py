"""Forms for authentication module"""
from django import forms
from django.utils.translation import ugettext_lazy as _
from naweza_main.functions import get_list, get_org_units_list

clinic_list = get_org_units_list('Please Select Unit')
sex_id_list = get_list('sex_id', 'Please Select')

class LoginForm(forms.Form):
    """Login form for the application."""

    username = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Username'),
               'class': 'form-control input-lg',
               'data-parsley-required': "true",
               'data-parsley-error-message': "Please enter your username.",
               'autofocus': 'true'}),
        error_messages={'required': 'Please enter your username.',
                        'invalid': 'Please enter a valid username.'})
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={'placeholder': _('Password'),
               'class': 'form-control input-lg',
               'data-parsley-required': "true",
               'data-parsley-error-message': "Please enter your password.",
               'autofocus': 'true'}),
        error_messages={'required': 'Please enter your password.',
                        'invalid': 'Please enter a valid password.'},)

    def clean_username(self):
        """Method to clean username."""
        username = self.cleaned_data['username']
        if not username:
            raise forms.ValidationError("Please enter your username.")
        return username

    def clean_password(self):
        """Method to clean password."""
        password = self.cleaned_data['password']
        if not password:
            raise forms.ValidationError("Please enter your password.")
        return password

class RegistrationForm(forms.Form):
    """Registration form."""

    first_name = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('First name'),
               'class': 'form-control',
               'id': 'first_name',
               'autofocus': 'true',
               'data-parsley-required': 'true'}))
    surname = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Surname'),
               'class': 'form-control',
               'id': 'surname',
               'autofocus': 'true',
               'data-parsley-required': 'true'})) 
    sex_id = forms.ChoiceField(
        choices=sex_id_list,
        widget=forms.Select(
            attrs={'placeholder': _('Sex'),
                   'class': 'form-control',
                   'id': 'sex_id',
                   'data-parsley-required': "true"}))   
    email = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Email'),
               'class': 'form-control',
               'id': 'email',
               'autofocus': 'true',
               'data-parsley-required': 'true'}))
    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        org_units_list = get_org_units_list('Please Select Clinic')

        clinic = forms.ChoiceField(
            choices=org_units_list,
            initial='0',
            widget=forms.Select(
                attrs={'class': 'form-control',
                    'id': 'clinic',
                    'data-parsley-required': 'true'
                  }))
        self.fields['clinic'] = clinic
    """
    clinic = forms.ChoiceField(choices=clinic_list,
        initial='0',
        widget=forms.Select(
          attrs={'class': 'form-control',
                'id': 'clinic',
               'data-parsley-required': 'true'
              }))
    """
    username = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Username'),
               'class': 'form-control',
               'id': 'username',
               'autofocus': 'true',
               'data-parsley-required': 'true'}))
    password1 = forms.CharField(widget=forms.PasswordInput(
        attrs={'placeholder': _('Password'),
               'class': 'form-control',
               'id': 'password1',
               'autofocus': 'true',
               'data-parsley-required': 'true'}))
    password2 = forms.CharField(widget=forms.PasswordInput(
        attrs={'placeholder': _('Re-enter password'),
               'class': 'form-control',
               'id': 'password2',
               'autofocus': 'true',
               'data-parsley-required': 'true'}))
    def clean_username(self):
        """Method to clean username."""
        try:
            AppUser.objects.get(username__iexact=self.cleaned_data['username'])
        except AppUser.DoesNotExist:
            return self.cleaned_data['username']
        raise forms.ValidationError(_(
            "The username already exists. Please try another one."))

    def clean(self):
        """Method to compare passwords."""
        form_obj = self.cleaned_data
        if 'password1' in form_obj and 'password2' in form_obj:
            if form_obj['password1'] != form_obj['password2']:
                raise forms.ValidationError(
                    _("The two password fields did not match."))
        return self.cleaned_data

class PasswordResetForm(forms.Form):
    """Registration form."""

    username = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Username'),
               'class': 'form-control',
               'autofocus': 'true'}))
    password1 = forms.CharField(widget=forms.PasswordInput(
        attrs={'placeholder': _('New Password'),
               'class': 'form-control',
               'autofocus': 'true'}))
    password2 = forms.CharField(widget=forms.PasswordInput(
        attrs={'placeholder': _('Re-enter New password'),
               'class': 'form-control',
               'autofocus': 'true'}))
    def clean_username(self):
        """Method to clean username."""
        try:
            AppUser.objects.get(username__iexact=self.cleaned_data['username'])
        except AppUser.DoesNotExist:            
          raise forms.ValidationError(_("The username does not exist. Please input your username if exists."))

    def clean(self):
        """Method to compare passwords."""
        form_obj = self.cleaned_data
        if 'password1' in form_obj and 'password2' in form_obj:
            if form_obj['password1'] != form_obj['password2']:
                raise forms.ValidationError(
                    _("The two password fields did not match."))
        return self.cleaned_data