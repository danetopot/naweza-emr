# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('naweza_auth', '0001_initial'),
        ('auth', '0006_require_contenttypes_0002'),
        ('naweza_registry', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='nawezauserrolegeoorg',
            name='org_unit',
            field=models.ForeignKey(related_name='reg_orgunit_fk', to='naweza_registry.RegOrgUnit', null=True),
        ),
        migrations.AddField(
            model_name='nawezauserrolegeoorg',
            name='user',
            field=models.ForeignKey(related_name='app_user_fk', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='appuser',
            name='groups',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups'),
        ),
        migrations.AddField(
            model_name='appuser',
            name='reg_person',
            field=models.OneToOneField(related_name='reg_person_fk', to='naweza_registry.RegPerson'),
        ),
        migrations.AddField(
            model_name='appuser',
            name='user_permissions',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions'),
        ),
    ]
