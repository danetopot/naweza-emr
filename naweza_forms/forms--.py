from django import forms
from django.utils.translation import ugettext_lazy as _
from django.forms.widgets import RadioFieldRenderer
from django.utils.encoding import force_unicode
from django.utils.safestring import mark_safe

from django import forms
from django.utils.translation import ugettext_lazy as _
from naweza_main.functions import get_list

sex_id_list = get_list('sex_id', 'Please Select')
yes_no_list = get_list('yes_no_id', 'Please Select')
diagnosis_type_list = get_list('diagnosis_type_id', 'Please Select')
investigations_type_list = get_list('investigation_type_id', 'Please Select')
medications_type_list = get_list('medication_type_id', 'Please Select')
referral_reason_list = get_list('referral_reason_id', 'Please Select')
referral_destination_list = get_list('referral_destination_id', 'Please Select')
form_type_list = get_list('form_type_id', 'Select Form Type')
YESNO_CHOICES = get_list('yesno_id')

class SearchForm(forms.Form):
    """Search registry form."""
    search_name = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Enter Patient Name .. '),
               'class': 'form-control',
               'id': 'search_name',
               'data-parsley-group': 'primary',
               'data-parsley-required': 'true'}))
    form_type = forms.ChoiceField(choices=form_type_list,
                        initial='0',
                        widget=forms.Select(
                          attrs={'class': 'form-control',
                          'id': 'form_type',
                          'data-parsley-group': 'primary',
                          'data-parsley-required': 'true'
                        }))

class RadioCustomRenderer(RadioFieldRenderer):
    """Custom radio button renderer class."""

    def render(self):
        """Renderer override method."""
        return mark_safe(u'%s' % u'\n'.join(
            [u'%s' % force_unicode(w) for w in self]))

class ClinicForm(forms.Form): 
    person_age = forms.CharField(widget=forms.TextInput(
        attrs={'type': 'hidden',
               'id': 'person_age'})) 
    visit_date = forms.DateField(widget=forms.TextInput(
        attrs={'placeholder': _('Visit Date'),
               'class': 'form-control',
               'id': 'visit_date',
               'data-parsley-required': "true",
               'data-parsley-group': 'group0'
               }))  
    visit_notes = forms.CharField(widget=forms.Textarea(
        attrs={'placeholder': _('Comments/Notes'),
               'class': 'form-control',
               'id': 'visit_notes',
               'rows': 3,
               'data-parsley-group': 'group0'}))

    # History
    medical_history = forms.MultipleChoiceField(
        choices=diagnosis_type_list,
        label=_('Please Select'),
        required=False,
        widget=forms.SelectMultiple(
             attrs={'class': 'form-control',
                          'id': 'medical_history',
                          'data-parsley-required': "true",
                          'data-parsley-group': 'group0'
                        }))

    # Diagnoses
    diagnosis_type = forms.ChoiceField(choices=diagnosis_type_list,
                        initial='0',
                        widget=forms.Select(
                          attrs={'class': 'form-control',
                          'id': 'diagnosis_type'
                        }))
    diagnosis_result = forms.ChoiceField(choices=yes_no_list,
                        initial='0',
                        widget=forms.Select(
                          attrs={'class': 'form-control',
                          'id': 'diagnosis_result'
                        }))
    diagnosis_result_other = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Other(specify)'),
               'class': 'form-control',
               'id': 'diagnosis_result_other'}))
    diagnosis_list = forms.CharField(widget=forms.TextInput(
        attrs={'type': 'hidden',
               'id': 'diagnosis_list'}))

    # Investigations
    investigation_type = forms.ChoiceField(choices=investigations_type_list,
                        initial='0',
                        widget=forms.Select(
                          attrs={'class': 'form-control',
                          'id': 'investigation_type'
                        }))
    investigation_result = forms.ChoiceField(choices=yes_no_list,
                        initial='0',
                        widget=forms.Select(
                          attrs={'class': 'form-control',
                          'id': 'investigation_result'
                        }))
    investigation_value = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Enter Value'),
               'class': 'form-control',
               'id': 'investigation_value'}))
    investigation_result_other = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Other(specify)'),
               'class': 'form-control',
               'id': 'investigation_result_other'}))
    investigations_list = forms.CharField(widget=forms.TextInput(
        attrs={'type': 'hidden',
               'id': 'investigations_list'}))

    # Medications
    medication_type = forms.ChoiceField(choices=medications_type_list,
                        initial='0',
                        widget=forms.Select(
                          attrs={'class': 'form-control',
                          'id': 'medication_type'
                        }))
    medication_dosage = forms.ChoiceField(choices=(),
                        initial='0',
                        widget=forms.Select(
                          attrs={'class': 'form-control',
                          'id': 'medication_dosage'
                        }))
    medication_dosage_other = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Other(specify)'),
               'class': 'form-control',
               'id': 'medication_dosage_other'}))
    medications_list = forms.CharField(widget=forms.TextInput(
        attrs={'type': 'hidden',
               'id': 'medications_list'}))

    # Referrals
    referral_present = forms.ChoiceField(
        choices=YESNO_CHOICES,
        widget=forms.RadioSelect(
            renderer=RadioCustomRenderer,
            attrs={'id': 'referral_present',
                   'data-parsley-required': 'true',
                   'data-parsley-errors-container': "#referral_error"}))
    referral_reason = forms.ChoiceField(choices=referral_reason_list,
                        initial='0',
                        widget=forms.Select(
                          attrs={'class': 'form-control',
                          'id': 'referral_reason'
                        }))
    referral_destination = forms.ChoiceField(choices=referral_destination_list,
                        initial='0',
                        widget=forms.Select(
                          attrs={'class': 'form-control',
                          'id': 'referral_destination'
                        }))
    referral_destination_other = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Other(specify)'),
               'class': 'form-control',
               'id': 'referral_destination_other'}))
    referrals_list = forms.CharField(widget=forms.TextInput(
        attrs={'type': 'hidden',
               'id': 'referrals_list'}))