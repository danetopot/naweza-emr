# Create your views here.
import simplejson as json
import uuid
import json
from dateutil.relativedelta import relativedelta
import datetime
import random
import ast
from django.shortcuts import render
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, JsonResponse
from naweza_main.models import SetupList
from naweza_main.functions import get_list_of_persons, get_list_of_orgs, get_dict, get_algorithm, get_drugs, convert_date, translate
from naweza_forms.functions import generate_history, load_obs, load_meds, load_tips, is_diabetic
from naweza.functions import generate_tips, generate_color, generate_risk
from naweza_registry.views import person_search, visits_search
from naweza_registry.forms import SearchForm
from naweza_registry.models import RegPersonsOrgUnits, RegOrgUnit, RegPerson, RegPersonsGeo, AfyaData, AfyaDataPersonInfo
from naweza_forms.forms import SearchForm as SearchForm_, ClinicForm
from naweza_forms.models import PatientVisits, PatientObs, PatientObs, PatientInvestigations, PatientMedications, PatientReferrals
from naweza_auth.models import AppUser

def archives(request):	
	form = SearchForm(data=request.POST)
	check_fields = ['sex_id']
	vals = get_dict(field_name=check_fields)


	try:
		if request.method == 'POST':
			search_string = request.POST.get('search_name')
			search_criteria = request.POST.get('search_criteria')
			resultsets = []
			personsets = get_list_of_persons(search_string=search_string, search_criteria=search_criteria)
			


			if not personsets:
				msg = 'No results for (%s).Person does not exist in database.' % search_string
				messages.add_message(request, messages.ERROR, msg)
				return render(request, 'forms/archives.html', {'form': form})

			for personset in personsets:
				_pvisits = PatientVisits.objects.filter(patient_id=personset.pk, is_void=False).order_by('-visit_date','patient_id')
				_pvisits.person = personset
				resultsets.append(_pvisits)

			for result in resultsets:
				print 'result.person %s' %result.person.fname
				for res in result:
					print 'res %s' % res.visit_date



			msg = 'Showing  results for (%s)' % search_string
			messages.add_message(request, messages.INFO, msg)
			return render(request, 'forms/archives.html',
			{ 
				'status': 400, 'form': form, 'vals': vals,
				'resultsets': resultsets
			})


			
		else:
			search_id = request.GET.get('person')
			search_criteria = 'PTPT'
			if search_id and search_id.isdigit():
				resultsets = get_list_of_persons(search_string=search_id, search_criteria=search_criteria)
				for resultset in resultsets:
					resultset.search_criteria = search_criteria

				return render(request, 'forms/archives.html',
				{ 
					'status': 400, 'form': form, 'vals': vals,
					'resultsets': resultsets
				})
				
	except Exception, e:
		raise e
	return render(request, 'forms/archives.html', { 'form': form })

def clinic_form(request):
	"""Some default page for Bad request error page."""
	form = SearchForm(data=request.POST)
	check_fields = ['sex_id']
	vals = get_dict(field_name=check_fields)
	resultsets = None

	try:
		if request.method == 'POST':
			resultsets = None
			search_string = request.POST.get('search_name')
			search_criteria = 'PTPT'
			org_unit_id = None

			resultsets = get_list_of_persons(
				search_string=search_string,
				search_criteria=search_criteria)
			print resultsets

			if not resultsets:
				msg = 'No results for (%s).Person does not exist in database.' % search_string
				messages.add_message(request, messages.ERROR, msg)
				return render(request, 'forms/clinic_form.html', {'form': form})

			for resultset in resultsets:
				# attach active/inactive status
				if search_criteria == 'PTUS':
					appuser = AppUser.objects.get(
						reg_person_id=resultset.person)
					resultset.appuser = appuser.is_active
					resultset.userid = appuser.id

				# attach org_unit
				org_units_att = RegPersonsOrgUnits.objects.filter(
					person=str(resultset.person))
				if org_units_att:
					for org_unit_att in org_units_att:
						org_unit_id = org_unit_att.org_unit_id
					org_unit = RegOrgUnit.objects.get(org_unit=org_unit_id)
					resultset.org_unit = org_unit
				resultset.search_criteria = search_criteria

			msg = 'Showing  results for (%s)' % search_string
			messages.add_message(request, messages.INFO, msg)
			return render(request,
						  'forms/clinic_form.html',
						  {
							  'status': 400,
							  'form': form,
							  'vals': vals,
							  'resultsets': resultsets
						  })
	except Exception, e:
		raise e
	return render(request, 'forms/clinic_form.html', {'form': form})


def new_followup(request, id):
	"""Some default page for Bad request error page."""
	init_data = AfyaData.objects.get(pk=id, active=True)
	pvisits = PatientVisits.objects.filter(patient=id, is_void=False).order_by('-visit_no')
	pmeds = PatientMedications.objects.filter(patient=id, is_void=False)
	pobs = PatientObs.objects.filter(patient=id, is_void=False)
	obs = PatientObs.objects.filter(patient=id, obs_name__in=['sbp', 'dbp','blood_sugar','blood_sugar_type','tobacco_use'], is_void=False).order_by('-date_created')

	check_fields = ['sex_id']
	vals = get_dict(field_name=check_fields)
	cur_visit_no = pvisits.count()

	try:
		if request.method == 'POST':
			obs = {}
			# pvisits = PatientVisits.objects.filter(patient=id, is_void=False)
			visit_no = (pvisits.count() + 1)

			# Visit Data
			visit_date = request.POST.get('visit_date')
			if visit_date:
				visit_date = convert_date(visit_date)

			pv = PatientVisits(
				visit_no=visit_no,
				visit_type_id='FOLLOW-UP',
				visit_date=visit_date,
				visit_notes= None,
				patient=AfyaData.objects.get(pk=id),
				created_by=AppUser.objects.get(pk=int(request.user.id)),
				rec_status = 'I')
			pv.save()
			visit_pk = pv.pk

			# Tab 1
			past_history = request.POST.getlist('past_history')
			past_history_other = request.POST.get('past_history_other')
			current_symptoms = request.POST.getlist('current_symptoms')
			current_symptoms_other = request.POST.get('current_symptoms_other')
			tobacco_use = request.POST.get('tobacco_use')
			physical_activity = request.POST.get('physical_activity')
			family_history = request.POST.get('family_history')
			activity_status = request.POST.get('activity_status')
			occupation = request.POST.get('occupation')
			current_medication = request.POST.get('current_medication')
			alcohal_consumption = request.POST.get('alcohal_consumption')
			alcohal_amount = request.POST.get('alcohal_amount')
			alcohal_frequency = request.POST.get('alcohal_frequency')
			obs['past_history'] = past_history
			obs['past_history_other'] = past_history_other
			obs['current_symptoms'] = current_symptoms
			obs['current_symptoms_other'] = current_symptoms_other
			obs['tobacco_use'] = tobacco_use
			obs['physical_activity'] = physical_activity
			obs['family_history'] = family_history
			obs['activity_status'] = activity_status
			obs['occupation'] = str(occupation).split(",") if occupation else None
			obs['current_medication'] = str(current_medication).split(",") if current_medication else None
			obs['alcohal_consumption'] = alcohal_consumption
			obs['alcohal_amount'] = alcohal_amount
			obs['alcohal_frequency'] = alcohal_frequency


			# Tab 2
			waist_circumference = request.POST.get('waist_circumference')
			sbp = request.POST.get('sbp')
			dbp = request.POST.get('dbp')
			oedema = request.POST.get('oedema')
			heaving = request.POST.get('heaving')
			displacement = request.POST.get('displacement')
			ausculate_heart = request.POST.get('ausculate_heart')
			ausculate_heart_describe = request.POST.get('ausculate_heart_describe')
			ausculate_lungs = request.POST.get('ausculate_lungs')
			ausculate_lungs_describe = request.POST.get('ausculate_lungs_describe')
			abdomen_exam = request.POST.get('abdomen_exam')
			abdomen_exam_describe = request.POST.get('abdomen_exam_describe')
			dm_exam = request.POST.get('dm_exam')
			dm_exam_describe = request.POST.get('dm_exam_describe')
			blood_sugar_type = request.POST.get('blood_sugar_type')
			bloodsugar = request.POST.get('bloodsugar')
			urine_ketones = request.POST.get('urine_ketones')
			urine_protein = request.POST.get('urine_protein')
			cholesterol = request.POST.get('cholesterol')
			assessment_comments = request.POST.get('assessment_comments')
			obs['waist_circumference'] = waist_circumference
			obs['sbp'] = sbp
			obs['dbp'] = dbp
			obs['oedema'] = oedema
			obs['heaving'] = heaving
			obs['displacement'] = displacement
			obs['ausculate_heart'] = ausculate_heart
			obs['ausculate_heart_describe'] = ausculate_heart_describe
			obs['ausculate_lungs'] = ausculate_lungs
			obs['ausculate_lungs_describe'] = ausculate_lungs_describe
			obs['abdomen_exam'] = abdomen_exam
			obs['abdomen_exam_describe'] = abdomen_exam_describe
			obs['dm_exam'] = dm_exam
			obs['dm_exam_describe'] = dm_exam_describe
			obs['blood_sugar_type'] = blood_sugar_type
			obs['blood_sugar'] = bloodsugar
			obs['urine_ketones'] = urine_ketones
			obs['urine_protein'] = urine_protein
			obs['cholesterol'] = cholesterol
			obs['assessment_comments'] = assessment_comments

			# Tab 3
			medications_list = request.POST.get('medications_list')
			
			# Tab 4
			referral_reason = request.POST.getlist('referral_reason')
			obs['referral_reason'] = referral_reason if referral_reason  else None

			# This list contains dict keys that are of array data types and will be processed
			# separately in a different save process
			list_ = ['past_history','current_symptoms','occupation','current_medication','prescription','referral_reason']

			for key, value in obs.iteritems():
				if value == '':
					value = None
				if value == 'AYES':
					value = 'YES'
				if value == 'ANNO':
					value = 'NO'

				if key not in list_:
					PatientObs(
						obs_name=key,
						obs_value=value,
						date_created = datetime.datetime.now().date(),
						visit= PatientVisits.objects.get(pk=str(visit_pk)),
						patient=AfyaData.objects.get(pk=id),
						).save()
				else:
					if value:
						for v in value:
							PatientObs(
								obs_name=key,
								obs_value= v, #str(v),
								date_created = datetime.datetime.now().date(),
								visit= PatientVisits.objects.get(pk=str(visit_pk)),
								patient=AfyaData.objects.get(pk=id),
								).save()
			# Save Medications			
			medications_data = json.loads(medications_list) if medications_list else None
			if medications_data:
				for medications in medications_data:
					medication_type = medications['medication_type']
					medication_name = medications['medication_name']
					medication_duration = medications['medication_duration']
					medication_dosage = medications['medication_dosage']
					medication_dosage_other = medications['medication_dosage_other']
					medication_dosage = medication_dosage if medication_dosage else medication_dosage_other

					PatientMedications(
						medication_type=medication_type,
						medication_name=medication_name,
						medication_duration=medication_duration,
						medication_dosage=medication_dosage,
						date_created = datetime.datetime.now().date(),
						visit= PatientVisits.objects.get(pk=str(visit_pk)),
						patient=AfyaData.objects.get(pk=id)
						).save()
			
			msg ='(%s %s) Data Saved Successfully!' % (init_data.fname, init_data.lname)
			messages.add_message(request, messages.INFO, msg)			
			return HttpResponseRedirect('%s?id=%s' % (reverse(visits_search), id))			
	except Exception, e:
		msg = 'Error saving form - (%s)' % str(e)
		messages.add_message(request, messages.ERROR, msg)
		return HttpResponseRedirect('%s?person=%s' % (reverse(person_search), id))

	# Init
	d = {}
	dates_added, edates, prev_obs, all_obs, visits, visit_dates, visits, _obs, stats, stats_i, stats_o = [], [], [], [], [], [], [], [], [], [], []
	no_of_visits = 0
	glucose_level, diabetes, color, date_created = None,None,None,None
	today = datetime.datetime.now().date()
	dob =  today - relativedelta(years=init_data.age)
	age = relativedelta(today, dob)	

	if pvisits:		
		# Get Visits/Obs/Medication
		for pvisit in pvisits:
			__pobs = pobs.filter(visit_id=pvisit.visit).values('obs_name', 'obs_value')
			__pmeds = pmeds.filter(visit_id=pvisit.visit)
			visits.append({
				'visit_no': pvisit.visit_no,
				'visit_id': pvisit.visit,
				'visit_date': (pvisit.visit_date).strftime('%d-%b-%Y'),
				'date_created': (pvisit.timestamp_created).strftime('%d-%b-%Y'),
				'patient_id': pvisit.patient_id,
				'patient_obs': load_obs(__pobs),
				'medication_type': load_meds(1, __pmeds),				
				'medication_dosage': load_meds(2, __pmeds)
				})
	
	if pobs:
		obs_prev = pobs.values('obs_name', 'obs_value')	
		for visit in pvisits:
			visit_date = visit.visit_date
			if str(visit_date) not in visit_dates:
				visit_dates.append(str(visit_date))
		

		# Get Previous Obs (Last Visit)
		obs_prev = obs.values('obs_name', 'obs_value')[:5]
		for ob in obs_prev:
			d[str(ob['obs_name'])] = str(ob['obs_value'])
		# print d
		glucose_level = d['blood_sugar']
		tobacco_use = d['tobacco_use']
		sbp = d['sbp']
		dbp = d['dbp']
		blood_sugar_type = d['blood_sugar_type']
		diabetes = is_diabetic(glucose_level, blood_sugar_type,None)
		d = datetime.datetime.strptime(visit_dates[0], '%Y-%m-%d')
		last_visit_date = datetime.date.strftime(d, "%m/%d/%Y")
		color = init_data.color = generate_color(age.years, init_data.sex, tobacco_use, diabetes, sbp)		

		# Get Previous Obs (ALL)
		for i, edate in enumerate(edates):
			randomnumber = random.randint(0, 3)
			badges = ['green', 'blue', 'purple', 'black']
			badge_color = badges[randomnumber]
			visit_obs = obs.filter(date_created=edate, obs_name__in=['sbp','dbp','tobacco_use','blood_sugar'])
			med_obs = pmeds.filter(date_created=edate)#.values()
			#pvisits = pvisits.filter(date_created=edate)#.values()
			#for medobs in med_obs:
			#	print medobs.medication_type
			all_obs.append({
				'visit_no': str(i+1),
				'visit_date': edate,
				'visit_obs': visit_obs,
				'pvisits': pvisits,
				'med_obs': med_obs,
				'badge_color': badge_color
			})
		#print all_obs

		# Generate History
		pHistory = generate_history(all_obs)

		form = ClinicForm({
			'color': init_data.color,
			'glucose_level': glucose_level,
			'person_age': init_data.age,
			'person_sex': init_data.sex,
			'patient_id': init_data.id,
			'visit_num': cur_visit_no + 1
			})		
		return render(request,
					  'forms/new_followup.html', {
						  'status': 200,
						  'form': form,
						  'init_data': init_data,
						  'vals': vals,
						  'age': age.years,
						  'dbp': dbp,
						  'sbp': sbp,
						  'smoker': tobacco_use,
						  'glucose': glucose_level,
						  'diabetes': diabetes,
						  'last_visit_date': last_visit_date,
						  'pHistory': pHistory,
						  'cur_visit_no': cur_visit_no+1,
						  'visits': visits
					  })
	else:		
		form = ClinicForm({
			'color': init_data.color,
			'glucose_level': glucose_level,
			'person_age': init_data.age,
			'person_sex': init_data.sex,
			'patient_id': init_data.id,
			'visit_num': 1
			})
		return render(request,
					  'forms/new_followup.html', {
						  'status': 200,
						  'form': form,
						  'init_data': init_data,
						  'cur_visit_no': 1,
						  # prev_obs/last_visit
						  'age': age.years,
						  'dbp': None,
						  'smoker': init_data.smoker,
						  'sbp': init_data.sbp,
						  'glucose': init_data.glucose,
						  'diabetes': init_data.diabetes,
						  'color': init_data.color,
						  'date_created': init_data.datecreated,
						  'pHistory': None,
						  'vals': vals
					  })


def edit_followup(request, uuid):
	"""Some default page for Bad request error page."""
	try:
		return render(request, '400.html', {'status': 400})
	except Exception, e:
		raise e


def view_followup(request, uuid):
	"""Some default page for Bad request error page."""
	try:
		return render(request, '400.html', {'status': 400})
	except Exception, e:
		raise e


def delete_followup(request, uuid):
	"""Some default page for Bad request error page."""
	try:
		return render(request, '400.html', {'status': 400})
	except Exception, e:
		raise e

def ref(request):
	return

def dss(request):
	jsonDict = {}
	__jsonData, jsonData, current_meds_list, visit_dates, visit_ids = [], [], [], [], []
	try:
		if request.method == 'POST':
			patient_id = request.POST.get('patient_id')
			age = request.POST.get('age')
			sex = request.POST.get('sex')
			dbp = request.POST.get('dbp')
			sbp = request.POST.get('sbp')
			bloodsugar = request.POST.get('bloodsugar')
			bloodsugartype = request.POST.get('bloodsugartype')
			past_history = request.POST.getlist('past_history[]')
			current_symptoms = request.POST.getlist('current_symptoms[]')
			symptoms = past_history+current_symptoms
			color = request.POST.get('color')
			visit_num = request.POST.get('visit_num')

			pvisits = PatientVisits.objects.filter(patient=patient_id, is_void=False).order_by('-visit_no')
			pmeds = PatientMedications.objects.filter(patient=patient_id, is_void=False)
			pobs = PatientObs.objects.filter(patient=patient_id, is_void=False)

			recent_pvisit = pvisits[0] if pvisits else None
			params = [patient_id,age,sex,sbp,dbp,bloodsugar,bloodsugartype,recent_pvisit,pmeds,visit_num,color,symptoms]
			medication_tips=load_tips(params)
			
			if pvisits:
				visit_id = recent_pvisit.visit				
				__recent_pmeds = pmeds.filter(visit_id=recent_pvisit.visit)
				for pvisit in pvisits:
					__pobs = pobs.filter(visit_id=pvisit.visit).values('obs_name', 'obs_value')
					__pmeds = pmeds.filter(visit_id=pvisit.visit)					
					__jsonData.append({
						'visit_no': pvisit.visit_no,
						'visit_id': pvisit.visit,
						'visit_date': (pvisit.visit_date).strftime('%d-%b-%Y'),
						'date_created': (pvisit.timestamp_created).strftime('%d-%b-%Y'),
						'patient_id': pvisit.patient_id,
						'patient_obs': load_obs(__pobs),
						'medication_type': load_meds(1, __pmeds),				
						'medication_dosage': load_meds(2, __pmeds),
						#'medication_tips': load_meds(3, __recent_pmeds)
						})
				jsonDict['jsonData'] = __jsonData
			jsonDict['jsonTips'] = medication_tips
			jsonData.append(jsonDict)
	except Exception, e:
		print 'DSS Error : - %s' % str(e)
	return JsonResponse(jsonData, content_type='application/json', safe=False)

def fetch_drug(request):
	jsonDrugsData = []
	try:
		if request.method == 'POST':
			itemid = request.POST.get('field_name')
			drugsubcategory = SetupList.objects.get(field_name='drug_type_id', item_id=itemid)
			item_sub_category = drugsubcategory.item_sub_category
			drugslist = SetupList.objects.filter(field_name=item_sub_category)

			# Append to JSON
			for drugs in drugslist:
				jsonDrugsData.append({
					'item_sub_category': drugs.item_description,
					'item_sub_category_id': drugs.item_id,
					'status': 1
				})
				
	except Exception as e:
		print 'Error Fetching Drugs : - %s' % str(e)	
	return JsonResponse(jsonDrugsData, content_type='application/json',
						safe=False)

def fetch_dose(request):
	jsonDosesData = []
	try:
		if request.method == 'POST':
			itemid = request.POST.get('field_name')
			dosesubcategory = SetupList.objects.get(field_name__iendswith='drugs_type', item_id=itemid)
			item_sub_category = dosesubcategory.item_sub_category
			doseslist = SetupList.objects.filter(field_name=item_sub_category)

			# Append to JSON
			for doses in doseslist:
				jsonDosesData.append({
					'item_sub_category': doses.item_description,
					'item_sub_category_id': doses.item_id,
					'status': 1
				})
				
	except Exception as e:
		print 'Error Fetching Dosages : - %s' % str(e)	
	return JsonResponse(jsonDosesData, content_type='application/json',
						safe=False)