from django.db import models
from django.utils import timezone
import datetime
import uuid
from naweza_registry.models import AfyaData, RegPerson, AppUser

# Create your models here.

class PatientVisits(models.Model):
	visit = models.UUIDField(
		primary_key=True, default=uuid.uuid1, editable=False)
	visit_type_id = models.CharField(max_length=20, null=True)
	visit_no = models.IntegerField(default=1)
	visit_date = models.DateField(null=True, blank=True, default=None)
	visit_notes = models.CharField(max_length=1000, null=True)
	timestamp_created = models.DateTimeField(default=timezone.now)	
	patient = models.ForeignKey(AfyaData)
	is_void = models.BooleanField(default=False)
	created_by = models.ForeignKey(AppUser, null=True)
	created_at = models.DateField(default=timezone.now)
	sync_id = models.UUIDField(default=uuid.uuid1, editable=False)
	rec_status = models.CharField(max_length=2, default='I', choices=[('I', 'Inserted'), ('D', 'Downloaded')])

	class Meta:
		db_table = 'patient_visits'

class PatientObs(models.Model):
	obs_id = models.UUIDField(
		primary_key=True, default=uuid.uuid1, editable=False)
	obs_name = models.CharField(max_length=50, blank=False)
	obs_value = models.CharField(max_length=1000, null=True)
	visit = models.ForeignKey(PatientVisits, on_delete=models.CASCADE)
	patient = models.ForeignKey(AfyaData)
	is_void = models.BooleanField(default=False)
	date_created = models.DateField(default=timezone.now)
	sync_id = models.UUIDField(default=uuid.uuid1, editable=False)
	rec_status = models.CharField(default='I', max_length=2)

	class Meta:
		db_table = 'patient_obs'

class PatientDiagnosis(models.Model):
	diagnosis_id = models.UUIDField(
		primary_key=True, default=uuid.uuid1, editable=False)
	diagnosis_type = models.CharField(max_length=4)
	diagnosis_result = models.CharField(max_length=4)
	diagnosis_result_other = models.CharField(max_length=1000, null=True)
	timestamp_created = models.DateTimeField(default=timezone.now)
	visit = models.ForeignKey(PatientVisits, on_delete=models.CASCADE)
	is_void = models.BooleanField(default=False)
	sync_id = models.UUIDField(default=uuid.uuid1, editable=False)
	rec_status = models.CharField(default='I', max_length=2)

	class Meta:
		db_table = 'patient_diagnosis'


class PatientInvestigations(models.Model):
	investigation_id = models.UUIDField(
		primary_key=True, default=uuid.uuid1, editable=False)
	investigation_type = models.CharField(max_length=4)
	investigation_result = models.CharField(max_length=20)
	timestamp_created = models.DateTimeField(default=timezone.now)
	visit = models.ForeignKey(PatientVisits, on_delete=models.CASCADE)
	is_void = models.BooleanField(default=False)
	sync_id = models.UUIDField(default=uuid.uuid1, editable=False)
	rec_status = models.CharField(default='I', max_length=2)

	class Meta:
		db_table = 'patient_investigations'

class PatientMedications(models.Model):
	medication_id = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	medication_type = models.CharField(max_length=4)
	medication_name = models.CharField(max_length=4, default=None)
	medication_dosage = models.CharField(max_length=100, default=None)
	medication_duration = models.IntegerField(default=0)
	visit = models.ForeignKey(PatientVisits, on_delete=models.CASCADE)
	patient = models.ForeignKey(AfyaData)
	is_void = models.BooleanField(default=False)
	date_created = models.DateField(default=timezone.now)
	sync_id = models.UUIDField(default=uuid.uuid1, editable=False)
	rec_status = models.CharField(default='I', max_length=2)

	class Meta:
		db_table = 'patient_medications'

class PatientReferrals(models.Model):
	referral_id = models.UUIDField(
		primary_key=True, default=uuid.uuid1, editable=False)
	referral_reason = models.CharField(max_length=4)
	referral_destination = models.CharField(max_length=4)
	timestamp_created = models.DateTimeField(default=timezone.now)
	visit = models.ForeignKey(PatientVisits, on_delete=models.CASCADE)
	is_void = models.BooleanField(default=False)
	sync_id = models.UUIDField(default=uuid.uuid1, editable=False)
	rec_status = models.CharField(default='I', max_length=2)

	class Meta:
		db_table = 'patient_referrals'

