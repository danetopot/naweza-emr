# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings
import uuid


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('naweza_registry', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PatientDiagnosis',
            fields=[
                ('diagnosis_id', models.UUIDField(default=uuid.uuid1, serialize=False, editable=False, primary_key=True)),
                ('diagnosis_type', models.CharField(max_length=4)),
                ('diagnosis_result', models.CharField(max_length=4)),
                ('diagnosis_result_other', models.CharField(max_length=1000, null=True)),
                ('timestamp_created', models.DateTimeField(default=django.utils.timezone.now)),
                ('is_void', models.BooleanField(default=False)),
                ('sync_id', models.UUIDField(default=uuid.uuid1, editable=False)),
                ('rec_status', models.CharField(default=b'I', max_length=2)),
            ],
            options={
                'db_table': 'patient_diagnosis',
            },
        ),
        migrations.CreateModel(
            name='PatientInvestigations',
            fields=[
                ('investigation_id', models.UUIDField(default=uuid.uuid1, serialize=False, editable=False, primary_key=True)),
                ('investigation_type', models.CharField(max_length=4)),
                ('investigation_result', models.CharField(max_length=20)),
                ('timestamp_created', models.DateTimeField(default=django.utils.timezone.now)),
                ('is_void', models.BooleanField(default=False)),
                ('sync_id', models.UUIDField(default=uuid.uuid1, editable=False)),
                ('rec_status', models.CharField(default=b'I', max_length=2)),
            ],
            options={
                'db_table': 'patient_investigations',
            },
        ),
        migrations.CreateModel(
            name='PatientMedications',
            fields=[
                ('medication_id', models.UUIDField(default=uuid.uuid1, serialize=False, editable=False, primary_key=True)),
                ('medication_type', models.CharField(max_length=4)),
                ('medication_name', models.CharField(default=None, max_length=4)),
                ('medication_dosage', models.CharField(default=None, max_length=100)),
                ('medication_duration', models.IntegerField(default=0)),
                ('is_void', models.BooleanField(default=False)),
                ('date_created', models.DateField(default=django.utils.timezone.now)),
                ('sync_id', models.UUIDField(default=uuid.uuid1, editable=False)),
                ('rec_status', models.CharField(default=b'I', max_length=2)),
                ('patient', models.ForeignKey(to='naweza_registry.AfyaData')),
            ],
            options={
                'db_table': 'patient_medications',
            },
        ),
        migrations.CreateModel(
            name='PatientObs',
            fields=[
                ('obs_id', models.UUIDField(default=uuid.uuid1, serialize=False, editable=False, primary_key=True)),
                ('obs_name', models.CharField(max_length=50)),
                ('obs_value', models.CharField(max_length=1000, null=True)),
                ('is_void', models.BooleanField(default=False)),
                ('date_created', models.DateField(default=django.utils.timezone.now)),
                ('sync_id', models.UUIDField(default=uuid.uuid1, editable=False)),
                ('rec_status', models.CharField(default=b'I', max_length=2)),
                ('patient', models.ForeignKey(to='naweza_registry.AfyaData')),
            ],
            options={
                'db_table': 'patient_obs',
            },
        ),
        migrations.CreateModel(
            name='PatientReferrals',
            fields=[
                ('referral_id', models.UUIDField(default=uuid.uuid1, serialize=False, editable=False, primary_key=True)),
                ('referral_reason', models.CharField(max_length=4)),
                ('referral_destination', models.CharField(max_length=4)),
                ('timestamp_created', models.DateTimeField(default=django.utils.timezone.now)),
                ('is_void', models.BooleanField(default=False)),
                ('sync_id', models.UUIDField(default=uuid.uuid1, editable=False)),
                ('rec_status', models.CharField(default=b'I', max_length=2)),
            ],
            options={
                'db_table': 'patient_referrals',
            },
        ),
        migrations.CreateModel(
            name='PatientVisits',
            fields=[
                ('visit', models.UUIDField(default=uuid.uuid1, serialize=False, editable=False, primary_key=True)),
                ('visit_type_id', models.CharField(max_length=20, null=True)),
                ('visit_no', models.IntegerField(default=1)),
                ('visit_date', models.DateField(default=None, null=True, blank=True)),
                ('visit_notes', models.CharField(max_length=1000, null=True)),
                ('timestamp_created', models.DateTimeField(default=django.utils.timezone.now)),
                ('is_void', models.BooleanField(default=False)),
                ('created_at', models.DateField(default=django.utils.timezone.now)),
                ('sync_id', models.UUIDField(default=uuid.uuid1, editable=False)),
                ('rec_status', models.CharField(default=b'I', max_length=2, choices=[(b'I', b'Inserted'), (b'D', b'Downloaded')])),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
                ('patient', models.ForeignKey(to='naweza_registry.AfyaData')),
            ],
            options={
                'db_table': 'patient_visits',
            },
        ),
        migrations.AddField(
            model_name='patientreferrals',
            name='visit',
            field=models.ForeignKey(to='naweza_forms.PatientVisits'),
        ),
        migrations.AddField(
            model_name='patientobs',
            name='visit',
            field=models.ForeignKey(to='naweza_forms.PatientVisits'),
        ),
        migrations.AddField(
            model_name='patientmedications',
            name='visit',
            field=models.ForeignKey(to='naweza_forms.PatientVisits'),
        ),
        migrations.AddField(
            model_name='patientinvestigations',
            name='visit',
            field=models.ForeignKey(to='naweza_forms.PatientVisits'),
        ),
        migrations.AddField(
            model_name='patientdiagnosis',
            name='visit',
            field=models.ForeignKey(to='naweza_forms.PatientVisits'),
        ),
    ]
