# functions.py
# coding=utf-8
import os
import xlrd
import json
import random
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from django.core.cache import cache
from django.db.models import Q, Count
from django.conf import settings
from django.http import HttpResponseRedirect, JsonResponse
from dateutil.relativedelta import relativedelta
from naweza_main.functions import translate
from naweza_main.models import SetupList

def load_obs(obs):
	data = []
	d = {}
	for o in obs:
		key = str(o['obs_name'])
		value = str(o['obs_value'])
		d[key] = value
		#data.append(d)
	return d

def load_meds(i, med_obs):	
	print 'med_obs %s' %med_obs
	medication_types, medication_dosages = [], []
	try:
		for med_ob in med_obs:
			medication_types.append(str(translate(med_ob.medication_type)))
			medication_dosages.append(
				str(
					translate(med_ob.medication_name) + ' - ' + translate(med_ob.medication_dosage) + ' for ' + str(med_ob.medication_duration) +' week(s)'
					))
		if i==1:
			mtypes =''
			for medication_type in medication_types:
				mtypes += '%s \n' % medication_type
				#medication_types = ','.join(medication_types)
			return mtypes
		if i==2:
			mdoses =''
			for medication_dosage in medication_dosages:
				mdoses += '%s \n' % medication_dosage
				#medication_dosages = ','.join(medication_dosages)
			return mdoses
		if i==3:
			mtips, mdict = [], []
			d = {}
			if med_obs:
				for med_ob in med_obs:
					medication_name=med_ob.medication_name
					medication_dosage=med_ob.medication_dosage
					medication_type=med_ob.medication_type

					setuplist = SetupList.objects.get(field_name__iendswith='dosage_type', item_id=medication_dosage, is_void=False)
					old_order = setuplist.the_order
					new_order = int(old_order) + 1
					field_name = setuplist.field_name
					mdict.append({
						'new_order': new_order,
						'field_name': field_name,
						'medication_name': medication_name
						})
				for md in mdict:
					new_order = md['new_order']
					field_name = md['field_name']
					medication_name = md['medication_name']
					setuplist = SetupList.objects.get(field_name=field_name, the_order=new_order, is_void=False)
					if setuplist:
						mtips.append('Increase %s Dosage To %s' %(translate(medication_name),setuplist.item_description_short))
					else:
						mtips.append('System Cannot Generate a New %s Dosage.Maximum Dosage Reached.' % translate(medication_name))
			else:
				mtips.append('System Cannot Generate a New %s Dosage.Maximum Dosage Reached.' % translate(medication_name))
			# print mtips
			return mtips
	except Exception as e:
		medication_types.append(str(translate(med_obs.medication_type)))
		medication_dosages.append(
			str(
				translate(med_obs.medication_name) + ' - ' + translate(med_obs.medication_dosage) + ' for ' + str(med_obs.medication_duration) +' week(s)'
				))
		if i==1:
			mtypes =''
			mtypes += '%s \n' % medication_types
			return mtypes
		if i==2:
			mdoses =''
			mdoses += '%s \n' % medication_dosages
			return mdoses
	

def generate_history(all_obs):
	obs, meds, medication_types, medication_dosages = [], [], [], []
	for all__ in all_obs:
		d = {}
		color, visit_no, visit_date, visit_obs, med_obs = all__['badge_color'], all__['visit_no'], all__['visit_date'], all__['visit_obs'], all__['med_obs']
		# print med_obs		
		d["color"] = color
		d["visit_no"] = visit_no
		d["visit_date"] = visit_date

		for visit_ob in visit_obs:
			d[str(visit_ob.obs_name)]=str(visit_ob.obs_value)

		for med_ob in med_obs:
			medication_types.append(str(translate(med_ob.medication_type)))
			medication_dosages.append(
				str(
					translate(med_ob.medication_name) + ' - ' + translate(med_ob.medication_dosage) + ' for ' + str(med_ob.medication_duration) +' week(s)'
					))

		data = {
			# 'color': color,
			'visit_no': visit_no,
			'visit_date': visit_date,
			'sbp': d['sbp'],
			'dbp': d['dbp'],
			'tobacco_use': d['tobacco_use'],
			'blood_sugarr': d['blood_sugar'],
			'medication_types' : medication_types,
			'medication_dosages': medication_dosages
			}
		obs.append(data)
	#print obs
	return obs

def is_diabetic(glucose_level):
	diabetes = 'YES' if glucose_level > 11.0 else 'NO'
	return diabetes

def obs_aggregate(d):
	obs_ = []
	obs_.append(d)
	print obs_
	return obs_
