# functions.py
# coding=utf-8
import os
import xlrd
import json
import random
from decimal import Decimal
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from django.core.cache import cache
from django.db.models import Q, Count
from django.conf import settings
from django.http import HttpResponseRedirect, JsonResponse
from dateutil.relativedelta import relativedelta
from naweza_main.functions import translate
from naweza_main.models import SetupList

def load_obs(obs):
	data = []
	d = {}
	for o in obs:
		key = str(o['obs_name'])
		value = str(o['obs_value'])
		d[key] = value
		#data.append(d)
	return d

def load_tips(params):
	mtips, mdict, mold_dict, mnew_dict = [], [], [], []
	d = {}
	if params:
		patient_id = params[0]
		age = int(params[1])
		sex = params[2]
		sbp = params[3]
		dbp = params[4]
		bloodsugar = params[5]
		bloodsugartype = params[6]
		pvisit = params[7]
		med_obs = params[8]
		visit_num = int(params[9])
		color = params[10]
		symptoms = params[11]
		diabetic = is_diabetic(sbp, dbp, bloodsugar, bloodsugartype, symptoms)
		diabetic = True if diabetic=='YES' else False
		hypertensive = is_hypertensive(sbp, dbp)

		if visit_num==1: 
			# Diabetic #
			if diabetic and hypertensive:
				mtip = 'Enalapril 5mg po od'
				if mtip not in mtips:
					mtips.append(mtip)


			# Female age 40-49.9, non-diabetic #
			if not diabetic and hypertensive:
				if sex=='F' and (age >=40 and age <50):
					mtip = 'Nifedipine 40mg po od'
					if mtip not in mtips:
						mtips.append(mtip)

			# Remaining Patients, non-diabetic males and [non-diabetic females age >=50 #
			if (not diabetic and sex=='M' or (sex=='F' and age >=50)) and hypertensive:
				mtip = 'Hydrochlorothiazide HCTZ 12.5mg po od'
				if mtip not in mtips:
					mtips.append(mtip)

			# DIABETES MEDICATIONS (ONLY FOR DIABETICS) #
			if diabetic and not hypertensive:
				mtip = 'Metformin 500mg po od'
				if mtip not in mtips:
					mtips.append(mtip)
		else:
			mtip = None

			# ASA #
			items = ['PHSK','PHTI','PHMC']
			if color=='RED' or (
				'PHSK' in symptoms or
				'PHTI' in symptoms or
				'PHMC' in symptoms):
				mtip = 'Aspirin(ASA) 75mg po od'
				if mtip not in mtips:				
					mtips.append(mtip)

			# ATORVASTATIN #
			if diabetic:
				mtip = 'Atorvastatin 20 mg po od'
				if mtip not in mtips:
					mtips.append(mtip)

			# Diabetic #
			if visit_num==2 and hypertensive:
				if diabetic:
					mtip = 'Enalapril 5mg po bid'
					if mtip not in mtips:
						mtips.append(mtip)
				else:
					mtip = 'Enalapril 5mg po od'
					if mtip not in mtips:
						mtips.append(mtip)
			if visit_num==3 and hypertensive:
				if diabetic:
					mtip = 'Enalapril 10mg po bid'
					if mtip not in mtips:
						mtips.append(mtip)
				else:
					mtip = 'Enalapril 5mg po bid'
					if mtip not in mtips:
						mtips.append(mtip)
			if visit_num==4 and hypertensive:
				if diabetic:
					mtip = 'Enalapril 10mg po bid + Nifedipine 40mg po od'
					if mtip not in mtips:
						mtips.append(mtip)
				else:
					mtip = 'Enalapril 10mg po bid'
					if mtip not in mtips:
						mtips.append(mtip)
			if visit_num==5 and hypertensive:
				if diabetic:
					mtip = 'Enalapril 10mg po bid + Nifedipine 80mg po od'
					if mtip not in mtips:
						mtips.append(mtip)
				else:
					mtip = 'Enalapril 10mg po bid + Nifedipine 40mg po od'
					if mtip not in mtips:
						mtips.append(mtip)
			if visit_num==6 and hypertensive:
				if diabetic:
					mtip = 'Enalapril 10mg po bid + Nifedipine 120mg po od'
					if mtip not in mtips:
						mtips.append(mtip)
				else:
					mtip = 'Enalapril 10mg po bid + Nifedipine 80mg po od'
					if mtip not in mtips:
						mtips.append(mtip)
			if visit_num > 6 and hypertensive:
				if diabetic:
					mtip = 'Consult internal medicine specialist'
					if mtip not in mtips:
						mtips.append(mtip)		

			# Female age 40-49.9, non-diabetic #
			if not diabetic and hypertensive:
				if sex=='F' and (age >=40 and age <50):
					if visit_num==2:
						mtip = 'Nifedipine 80mg po od'
						if mtip not in mtips:
							mtips.append(mtip)
					if visit_num==3:
						mtip = 'Nifedipine 120mg po od'
						if mtip not in mtips:
							mtips.append(mtip)
					if visit_num==4:
						mtip = 'Nifedipine 120mg po od + Atenolol 50mg po od'
						if mtip not in mtips:
							mtips.append(mtip)
					if visit_num==5:
						mtip = 'Nifedipine 120mg po od + Atenolol 50mg po bid'
						if mtip not in mtips:
							mtips.append(mtip)
					if visit_num > 5:
						mtip = 'Consult internal medicine specialist'
						if mtip not in mtips:
							mtips.append(mtip)

			# Remaining Patients, non-diabetic males and [non-diabetic females age >=50 #
			if (not diabetic and sex=='M' or (sex=='F' and age >=50))  and hypertensive:
				if visit_num==2:
					mtip = 'Hydrochlorothiazide HCTZ 25mg po od'
					if mtip not in mtips:
						mtips.append(mtip)
				if visit_num==3:
					mtip = 'Hydrochlorothiazide HCTZ 25mg po od + Enalapril 5mg po od'
					if mtip not in mtips:
						mtips.append(mtip)
				if visit_num==4:
					mtip = 'Hydrochlorothiazide HCTZ 25mg po od + Enalapril 5mg po bid'
					if mtip not in mtips:
						mtips.append(mtip)
				if visit_num==5:
					mtip = 'Hydrochlorothiazide HCTZ 25mg po od + Enalapril 10mg po bid'
					if mtip not in mtips:
						mtips.append(mtip)
				if visit_num==6:
					mtip = 'Hydrochlorothiazide HCTZ 25mg po od+ Enalapril 10mg po bid+ Nifedipine 40mg po od'
					if mtip not in mtips:
						mtips.append(mtip)
				if visit_num==7:
					mtip = 'Hydrochlorothiazide HCTZ 25mg po od+ Enalapril 10mg po bid+ Nifedipine 80mg po od'
					if mtip not in mtips:
						mtips.append(mtip)
				if visit_num==8:
					mtip = 'Hydrochlorothiazide HCTZ 25mg po od+ Enalapril 10mg po bid+ Nifedipine 120mg po od'
					if mtip not in mtips:
						mtips.append(mtip)
				if visit_num > 8:
						mtip = 'Consult internal medicine specialist'
						if mtip not in mtips:
							mtips.append(mtip)

			# DIABETES MEDICATIONS (ONLY FOR DIABETICS) #
			if diabetic and not hypertensive:
				if visit_num==2:
					mtip = 'Metformin 1g po od'
					if mtip not in mtips:
						mtips.append(mtip)
				if visit_num==3:
					mtip = 'Metformin 1g po bid'
					if mtip not in mtips:
						mtips.append(mtip)
				if visit_num==4:
					mtip = 'Metformin 1g po bid + Glybenclamide 2.5mg po bid'
					if mtip not in mtips:
						mtips.append(mtip)
				if visit_num==5:
					mtip = 'Metformin 1g po bid + Glybenclamide 5mg po bid'
					if mtip not in mtips:
						mtips.append(mtip)
				if visit_num==6:
					mtip = 'Metformin 1g po bid + Glybenclamide 7.5mg po bid'
					if mtip not in mtips:
						mtips.append(mtip)
				if visit_num > 6:
					mtip = 'Consult internal medicine specialist'
					if mtip not in mtips:
						mtips.append(mtip)

			if len(mtips)==0:
				mtips.append('The Prescription Support System could not provide tips at the moment.\nPlease try the following :- \n (a) Input Patient Assessment Data\n (b) Contact Internal Medicine Specialist in the Clinic')			
	return mtips

def load_meds(i, med_obs):	
	#print 'med_obs %s' %med_obs
	medication_types, medication_dosages = [], []
	try:
		if int(i) in [1,2]:
			for med_ob in med_obs:
				medication_types.append(str(translate(med_ob.medication_type)))
				medication_dosages.append(
					str(
						translate(med_ob.medication_name) + ' - ' + translate(med_ob.medication_dosage) + ' for ' + str(med_ob.medication_duration) +' week(s)'
						))
		if i==1:
			mtypes =''
			for medication_type in medication_types:
				mtypes += '%s \n' % medication_type
				#medication_types = ','.join(medication_types)
			return mtypes
		if i==2:
			mdoses =''
			for medication_dosage in medication_dosages:
				mdoses += '%s \n' % medication_dosage
				#medication_dosages = ','.join(medication_dosages)
			return mdoses
	except Exception as e:
		medication_types.append(str(translate(med_obs.medication_type)))
		medication_dosages.append(
			str(
				translate(med_obs.medication_name) + ' - ' + translate(med_obs.medication_dosage) + ' for ' + str(med_obs.medication_duration) +' week(s)'
				))
		if i==1:
			mtypes =''
			mtypes += '%s \n' % medication_types
			return mtypes
		if i==2:
			mdoses =''
			mdoses += '%s \n' % medication_dosages
			return mdoses
	

def generate_history(all_obs):
	obs, meds, medication_types, medication_dosages = [], [], [], []
	if all_obs:
		for all__ in all_obs:
			d = {}
			color, visit_no, visit_date, visit_obs, med_obs = all__['badge_color'], all__['visit_no'], all__['visit_date'], all__['visit_obs'], all__['med_obs']
			# print med_obs		
			d["color"] = color
			d["visit_no"] = visit_no
			d["visit_date"] = visit_date

			for visit_ob in visit_obs:
				d[str(visit_ob.obs_name)]=str(visit_ob.obs_value)

			for med_ob in med_obs:
				medication_types.append(str(translate(med_ob.medication_type)))
				medication_dosages.append(
					str(
						translate(med_ob.medication_name) + ' - ' + translate(med_ob.medication_dosage) + ' for ' + str(med_ob.medication_duration) +' week(s)'
						))

			data = {
				# 'color': color,
				'visit_no': visit_no,
				'visit_date': visit_date,
				'sbp': d['sbp'],
				'dbp': d['dbp'],
				'tobacco_use': d['tobacco_use'],
				'blood_sugarr': d['blood_sugar'],
				'medication_types' : medication_types,
				'medication_dosages': medication_dosages
				}
			obs.append(data)
	#print obs
	return obs

def is_diabetic(sbp, dbp, bloodsugar, bloodsugartype, pasthistory):
	# print 'sbp %s, dbp %s, bloodsugar %s, bloodsugartype %s' %(type(sbp), dbp, bloodsugar, bloodsugartype)
	sbp = int(sbp) if sbp else None
	dbp = int(dbp)  if dbp else None
	bloodsugar = int(bloodsugar) if bloodsugar else None
	diabetes = 'NO'
	if sbp or dbp or bloodsugar or pasthistory:
		if sbp>130 or dbp>80 or (bloodsugar>11 and bloodsugartype=='STRA') or (bloodsugar>7 and bloodsugartype=='STFA') or ('PHDM' in pasthistory):
			diabetes = 'YES'
	return diabetes

def is_hypertensive(sbp, dbp):
	# print 'sbp %s, dbp %s, bloodsugar %s, bloodsugartype %s' %(type(sbp), dbp, bloodsugar, bloodsugartype)
	sbp = int(sbp) if sbp else None
	dbp = int(dbp)  if dbp else None
	hypertensive = False
	if sbp or dbp:
		if sbp>140 or dbp>90:
			hypertensive=True
	return hypertensive

def obs_aggregate(d):
	obs_ = []
	obs_.append(d)
	print obs_
	return obs_
