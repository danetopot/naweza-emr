from django import forms
from django.utils.translation import ugettext_lazy as _
from django.forms.widgets import RadioFieldRenderer
from django.utils.encoding import force_unicode
from django.utils.safestring import mark_safe

from django import forms
from django.utils.translation import ugettext_lazy as _
from naweza_main.functions import get_list

activity_status_list = get_list('activity_status_id')
bloodsugar_type_list = get_list('bloodsugar_type_id')
conditions_list = get_list('condition_id')
sex_id_list = get_list('sex_id', 'Please Select')
yes_no_list = get_list('yes_no_id', 'Please Select')
diagnosis_type_list = get_list('diagnosis_type_id', 'Please Select')
investigations_type_list = get_list('investigation_type_id', 'Please Select')
medications_type_list = get_list('drug_type_id', 'Please Select')
pasthistory_type_list = get_list('pasthistory_type_id', 'Please Select')
currentsymptoms_list = get_list('currentsymptoms_id', 'Please Select')
risk_level_list = get_list('risk_level_id', 'Please Select')
referral_reason_list = get_list('referral_reason_id', 'Please Select')
referral_destination_list = get_list('referral_destination_id', 'Please Select')
form_type_list = get_list('form_type_id', 'Select Form Type')
YESNO_CHOICES = get_list('yesno_id')

class SearchForm(forms.Form):
    """Search registry form."""
    search_name = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Enter Patient Name .. '),
               'class': 'form-control',
               'id': 'search_name',
               'data-parsley-group': 'primary',
               'data-parsley-required': 'true'}))
    form_type = forms.ChoiceField(choices=form_type_list,
                        initial='0',
                        widget=forms.Select(
                          attrs={'class': 'form-control',
                          'id': 'form_type',
                          'data-parsley-group': 'primary',
                          'data-parsley-required': 'true'
                        }))

class RadioCustomRenderer(RadioFieldRenderer):
    """Custom radio button renderer class."""

    def render(self):
        """Renderer override method."""
        return mark_safe(u'%s' % u'\n'.join(
            [u'%s' % force_unicode(w) for w in self]))

class ClinicForm(forms.Form): 
    color = forms.CharField(widget=forms.TextInput(
        attrs={ 'type': 'hidden',
               'id': 'color'}))
    glucose_level = forms.CharField(widget=forms.TextInput(
        attrs={ 'type': 'hidden',
               'id': 'glucose_level'})) 
    person_sex = forms.CharField(widget=forms.TextInput(
        attrs={ 'type': 'hidden',
               'id': 'person_sex'}))
    person_age = forms.CharField(widget=forms.TextInput(
        attrs={ 'type': 'hidden',
               'id': 'person_age'})) 
    patient_id = forms.CharField(widget=forms.TextInput(
        attrs={ 'type': 'hidden',
               'id': 'patient_id'})) 
    visit_num = forms.CharField(widget=forms.TextInput(
        attrs={ 'type': 'hidden',
               'id': 'visit_num'})) 
    visit_date = forms.DateField(widget=forms.TextInput(
        attrs={'placeholder': _('Visit Date'),
               'class': 'form-control',
               'id': 'visit_date',
               'data-parsley-required': "true",
               'data-parsley-group': 'group0'
               }))  
    visit_notes = forms.CharField(widget=forms.Textarea(
        attrs={'placeholder': _('Comments/Notes'),
               'class': 'form-control',
               'id': 'visit_notes',
               'rows': 3,
               'data-parsley-group': 'group0'}))

    # Tab 1
    past_history = forms.MultipleChoiceField(
        choices=pasthistory_type_list,
        label=_('Please Select'),
        required=False,
        widget=forms.SelectMultiple(
             attrs={'class': 'form-control',
                          'id': 'past_history',
                          'data-parsley-required': "true",
                          'data-parsley-group': 'group0'
                        }))
    past_history_other = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Other(Specify)'),
               'class': 'form-control',
               'id': 'past_history_other',
              'data-parsley-required': "false",
              'data-parsley-group': 'group0'}))
    current_symptoms = forms.MultipleChoiceField(
        choices=currentsymptoms_list,
        label=_('Please Select'),
        required=False,
        widget=forms.SelectMultiple(
             attrs={'class': 'form-control',
                          'id': 'current_symptoms',
                          'data-parsley-required': "true",
                          'data-parsley-group': 'group0'
                        }))
    current_symptoms_other = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Other(Specify)'),
               'class': 'form-control',
               'id': 'current_symptoms_other',
              'data-parsley-required': "false",
              'data-parsley-group': 'group0'}))
    tobacco_use = forms.ChoiceField(
        choices=YESNO_CHOICES,
        widget=forms.RadioSelect(
            renderer=RadioCustomRenderer,
            attrs={'id': 'tobacco_use',
                   'data-parsley-required': 'true',
                   'data-parsley-group': 'group0',
                   'data-parsley-errors-container': "#tobacco_use_error"}))
    alcohal_consumption = forms.ChoiceField(
        choices=YESNO_CHOICES,
        widget=forms.RadioSelect(
            renderer=RadioCustomRenderer,
            attrs={'id': 'alcohal_consumption',
                   'data-parsley-required': 'true',
                   'data-parsley-group': 'group0',
                   'data-parsley-errors-container': "#alcohal_consumption_error"}))
    alcohal_amount = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Alcohal Amount'),
               'class': 'form-control',
               'id': 'alcohal_amount',
              'data-parsley-required': "false",
              'data-parsley-group': 'group0'}))
    alcohal_frequency = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Frequency'),
               'class': 'form-control',
               'id': 'alcohal_frequency',
                'data-parsley-required': "false",
                'data-parsley-group': 'group0'}))
    activity_status = forms.ChoiceField(
        choices=activity_status_list,
        widget=forms.RadioSelect(
            renderer=RadioCustomRenderer,
            attrs={'id': 'activity_status',
                   'data-parsley-required': 'true',
                   'data-parsley-group': 'group0',
                   'data-parsley-errors-container': "#activity_status_error"}))
    physical_activity = forms.ChoiceField(
        choices=YESNO_CHOICES,
        widget=forms.RadioSelect(
            renderer=RadioCustomRenderer,
            attrs={'id': 'physical_activity',
                   'data-parsley-required': 'true',
                   'data-parsley-group': 'group0',
                   'data-parsley-errors-container': "#physical_activity_error"}))
    family_history = forms.ChoiceField(
        choices=YESNO_CHOICES,
        widget=forms.RadioSelect(
            renderer=RadioCustomRenderer,
            attrs={'id': 'family_history',
                   'data-parsley-required': 'true',
                   'data-parsley-group': 'group0',
                   'data-parsley-errors-container': "#family_history_error"}))
    current_medication = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Medicines the patient is taking'),
               'class': 'form-control',
               'id': 'current_medication',
               # 'rows': 2,
               'data-parsley-required': 'false',
               'data-parsley-group': 'group0'}))

    occupation = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Describe Occupation'),
               'class': 'form-control',
               'id': 'occupation',
               # 'rows': 2,
               'data-parsley-required': 'false',
               'data-parsley-group': 'group0'}))

    # Tab2
    waist_circumference  = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _('Waist Circumference'),
               'class': 'form-control',
               'id': 'alcohal_frequency',
               'data-parsley-type': "digits",
                'data-parsley-required': "false",
                'data-parsley-group': 'group1'}))
    sbp  = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _('Systolic Blood Pressure'),
               'class': 'form-control',
               'id': 'sbp',
               'data-parsley-type': "digits",
                'data-parsley-required': "true",
                'data-parsley-group': 'group1'}))
    dbp  = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _('Diastolic Blood Pressure'),
               'class': 'form-control',
               'id': 'dbp',
               'data-parsley-type': "digits",
                'data-parsley-required': "true",
                'data-parsley-group': 'group1'}))
    oedema = forms.ChoiceField(
        choices=YESNO_CHOICES,
        widget=forms.RadioSelect(
            renderer=RadioCustomRenderer,
            attrs={'id': 'oedema',
                   'data-parsley-required': 'false',
                   'data-parsley-group': 'group1',
                   'data-parsley-errors-container': "#oedema_error"}))
    heaving = forms.ChoiceField(
        choices=YESNO_CHOICES,
        widget=forms.RadioSelect(
            renderer=RadioCustomRenderer,
            attrs={'id': 'heaving',
                   'data-parsley-required': 'false',
                   'data-parsley-group': 'group1',
                   'data-parsley-errors-container': "#heaving_error"}))
    displacement = forms.ChoiceField(
        choices=YESNO_CHOICES,
        widget=forms.RadioSelect(
            renderer=RadioCustomRenderer,
            attrs={'id': 'displacement',
                   'data-parsley-required': 'false',
                   'data-parsley-group': 'group1',
                   'data-parsley-errors-container': "#displacement_error"}))
    ausculate_heart = forms.ChoiceField(
        choices=conditions_list,
        widget=forms.RadioSelect(
            renderer=RadioCustomRenderer,
            attrs={'id': 'ausculate_heart',
                   'data-parsley-required': 'false',
                   'data-parsley-group': 'group1',
                   'data-parsley-errors-container': "#ausculate_heart_error"}))
    ausculate_heart_describe = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _('Describe Ausculate Heart Findings'),
               'class': 'form-control',
               'id': 'ausculate_heart_describe',
                'data-parsley-required': "false",
                'data-parsley-group': 'group1'}))
    ausculate_lungs = forms.ChoiceField(
        choices=YESNO_CHOICES,
        widget=forms.RadioSelect(
            renderer=RadioCustomRenderer,
            attrs={'id': 'ausculate_lungs',
                   'data-parsley-required': 'false',
                   'data-parsley-group': 'group1',
                   'data-parsley-errors-container': "#ausculate_lungs_error"}))
    ausculate_lungs_describe = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _('Describe Ausculate Lungs Findings'),
               'class': 'form-control',
               'id': 'ausculate_lungs_describe',
                'data-parsley-required': "false",
                'data-parsley-group': 'group1'}))
    abdomen_exam = forms.ChoiceField(
        choices=YESNO_CHOICES,
        widget=forms.RadioSelect(
            renderer=RadioCustomRenderer,
            attrs={'id': 'abdomen_exam',
                   'data-parsley-required': 'false',
                   'data-parsley-group': 'group1',
                   'data-parsley-errors-container': "#abdomen_exam_error"}))
    abdomen_exam_describe = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _('Describe Abdomen Exam Findings'),
               'class': 'form-control',
               'id': 'abdomen_exam_describe',
                'data-parsley-required': "false",
                'data-parsley-group': 'group1'}))
    dm_exam = forms.ChoiceField(
        choices=conditions_list,
        widget=forms.RadioSelect(
            renderer=RadioCustomRenderer,
            attrs={'id': 'dm_exam',
                   'data-parsley-required': 'false',
                   'data-parsley-group': 'group1',
                   'data-parsley-errors-container': "#dm_exam_error"}))
    dm_exam_describe = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _('Describe DM Exam'),
               'class': 'form-control',
               'id': 'dm_exam_describe',
                'data-parsley-required': "false",
                'data-parsley-group': 'group1'}))
    blood_sugar_type = forms.ChoiceField(
        choices=bloodsugar_type_list,
        widget=forms.RadioSelect(
            renderer=RadioCustomRenderer,
            attrs={'id': 'blood_sugar_type',
                   'data-parsley-required': 'true',
                   'data-parsley-group': 'group1',
                   'data-parsley-errors-container': "#blood_sugar_type_error"}))
    blood_sugar = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _('Fasting/Blood Sugar Level'),
               'class': 'form-control',
               'id': 'blood_sugar',
               'data-parsley-type': "digits",
                'data-parsley-required': "true",
                'data-parsley-group': 'group1'}))
    bloodsugar  = forms.DecimalField(widget=forms.TextInput(attrs={'placeholder': _('Blood Sugar'),
               'class': 'form-control',
               'id': 'bloodsugar',
               'data-parsley-type': "number",
                'data-parsley-required': "true",
                'data-parsley-group': 'group1',
                'decimal_places': 1}))
    urine_ketones = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _('Urine Ketones'),
               'class': 'form-control',
               'id': 'urine_ketones',
               'data-parsley-type': "number",
                'data-parsley-required': "false",
                'data-parsley-group': 'group1'}))
    urine_protein = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _('Urine Protein'),
               'class': 'form-control',
               'id': 'urine_protein',
               'data-parsley-type': "number",
                'data-parsley-required': "false",
                'data-parsley-group': 'group1'}))
    cholesterol = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _('Cholesterol'),
               'class': 'form-control',
               'id': 'cholesterol',
               'data-parsley-type': "number",
                'data-parsley-required': "false",
                'data-parsley-group': 'group1'}))
    risk_level = forms.ChoiceField(
        choices=risk_level_list,
        label=_('Please Select'),
        required=False,
        widget=forms.Select(
             attrs={'class': 'form-control',
                          'id': 'risk_level',
                          'data-parsley-required': "true",
                          'data-parsley-group': 'group1'
                        }))
    assessment_comments = forms.CharField(widget=forms.Textarea(attrs={'placeholder': _('Comments/Notes'),
               'class': 'form-control',
               'id': 'ausculate_lungs_describe',
                'data-parsley-group': 'group1',
                'rows': 3 }))

    # Tab 3
    chk_prescription = forms.CharField(
        widget=forms.CheckboxInput(
            attrs={'autofocus': 'false'}))
    prescription = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Medicines Prescribed'),
               'class': 'form-control',
               'id': 'prescription',
               'data-parsley-required': "false",
               'data-parsley-group': 'group2'}))
    medication_type = forms.ChoiceField(
      choices=medications_type_list,
      initial='0',
        widget=forms.Select(
        attrs={'class': 'form-control',
        'id': 'medication_type'
      }))
    medication_name = forms.ChoiceField(
      choices=(),
      initial='0',
      widget=forms.Select(
        attrs={'class': 'form-control',
        'id': 'medication_name'
      }))
    medication_dosage = forms.ChoiceField(
      choices=(),
      initial='0',
      widget=forms.Select(
        attrs={'class': 'form-control',
        'id': 'medication_dosage'
      }))
    medication_dosage_other = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Other(specify)'),
               'class': 'form-control',
               'id': 'medication_dosage_other'}))
    medication_duration = forms.IntegerField(
      widget=forms.NumberInput(
        attrs={'class': 'form-control',
        'id': 'medication_duration',
        'min': 0

      }))
    medications_list = forms.CharField(widget=forms.TextInput(
        attrs={'type': 'hidden',
               'id': 'medications_list'}))

    # Tab 4
    chk_referral = forms.CharField(
        widget=forms.CheckboxInput(
            attrs={'autofocus': 'false'}))
    referral_reason = forms.MultipleChoiceField(
        choices=referral_reason_list,
        label=_('Please Select'),
        required=False,
        widget=forms.SelectMultiple(
             attrs={'class': 'form-control',
                          'id': 'referral_reason',
                          #'data-parsley-required': "true",
                          'data-parsley-group': 'group3'
                        }))
    referral_reason_other = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Other(Specify)'),               
               'id': 'referral_reason_other',
               'data-parsley-required': "false",
               'type': 'hidden',
               'rows': 3,
               'class': 'form-control'}))

