from django.db import models
from django.utils import timezone
import datetime
import uuid
from naweza_registry.models import RegPerson, AppUser

# Create your models here.

class PatientVisits(models.Model):
	visit = models.UUIDField(
		primary_key=True, default=uuid.uuid1, editable=False)
	visit_type_id = models.CharField(max_length=20)
	visit_no = models.IntegerField(default=1)
	visit_date = models.DateField(null=True, blank=True, default=None)
	visit_notes = models.CharField(max_length=1000, blank=True)
	timestamp_created = models.DateTimeField(default=timezone.now)
	is_void = models.BooleanField(default=False)
	sync_id = models.UUIDField(default=uuid.uuid1, editable=False)
	app_user = models.ForeignKey(AppUser, default=1)
	person = models.ForeignKey(RegPerson)
	rec_status = models.CharField(default='I', max_length=2)

	class Meta:
		db_table = 'patient_visits'

class PatientObs(models.Model):
	obs_id = models.UUIDField(
		primary_key=True, default=uuid.uuid1, editable=False)
	obs_name = models.CharField(max_length=50, blank=False)
	obs_value = models.CharField(max_length=1000, null=False)
	timestamp_created = models.DateTimeField(default=timezone.now)
	visit = models.ForeignKey(PatientVisits, on_delete=models.CASCADE)
	is_void = models.BooleanField(default=False)
	sync_id = models.UUIDField(default=uuid.uuid1, editable=False)
	rec_status = models.CharField(default='I', max_length=2)

	class Meta:
		db_table = 'patient_obs'

""""
class PatientDiagnosis(models.Model):
	diagnosis_id = models.UUIDField(
		primary_key=True, default=uuid.uuid1, editable=False)
	diagnosis_type = models.CharField(max_length=4)
	diagnosis_result = models.CharField(max_length=4)
	diagnosis_result_other = models.CharField(max_length=1000, null=True)
	timestamp_created = models.DateTimeField(default=timezone.now)
	visit = models.ForeignKey(PatientVisits, on_delete=models.CASCADE)
	is_void = models.BooleanField(default=False)
	sync_id = models.UUIDField(default=uuid.uuid1, editable=False)
	rec_status = models.CharField(default='I', max_length=2)

	class Meta:
		db_table = 'patient_diagnosis'


class PatientInvestigations(models.Model):
	investigation_id = models.UUIDField(
		primary_key=True, default=uuid.uuid1, editable=False)
	investigation_type = models.CharField(max_length=4)
	investigation_result = models.CharField(max_length=20)
	timestamp_created = models.DateTimeField(default=timezone.now)
	visit = models.ForeignKey(PatientVisits, on_delete=models.CASCADE)
	is_void = models.BooleanField(default=False)
	sync_id = models.UUIDField(default=uuid.uuid1, editable=False)
	rec_status = models.CharField(default='I', max_length=2)

	class Meta:
		db_table = 'patient_investigations'

class PatientMedications(models.Model):
	medication_id = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	medication_type = models.CharField(max_length=4)
	medication_dosage = models.CharField(max_length=4)
	timestamp_created = models.DateTimeField(default=timezone.now)
	visit = models.ForeignKey(PatientVisits, on_delete=models.CASCADE)
	is_void = models.BooleanField(default=False)
	sync_id = models.UUIDField(default=uuid.uuid1, editable=False)
	rec_status = models.CharField(default='I', max_length=2)

	class Meta:
		db_table = 'patient_medications'

class PatientReferrals(models.Model):
	referral_id = models.UUIDField(
		primary_key=True, default=uuid.uuid1, editable=False)
	referral_reason = models.CharField(max_length=4)
	referral_destination = models.CharField(max_length=4)
	timestamp_created = models.DateTimeField(default=timezone.now)
	visit = models.ForeignKey(PatientVisits, on_delete=models.CASCADE)
	is_void = models.BooleanField(default=False)
	sync_id = models.UUIDField(default=uuid.uuid1, editable=False)
	rec_status = models.CharField(default='I', max_length=2)

	class Meta:
		db_table = 'patient_referrals'
""""