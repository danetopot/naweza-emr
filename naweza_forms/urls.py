"""URLs for authentication module."""
from django.conf.urls import patterns, url

# This should contain urls related to auth app ONLY
urlpatterns = patterns('naweza_forms.views',
		url(r'^archives/$', 'archives', name='archives'),
		url(r'^clinic_form/$', 'clinic_form', name='clinic_form'),
		# url(r'^visits/$', 'visits', name='visits'),
		url(r'^new_followup/(?P<id>\d+)/$', 'new_followup', name='new_followup'),
		url(r'^edit_followup/(?P<id>[0-9a-z-]+)/$', 'edit_followup', name='edit_followup'),
		url(r'^view_followup/(?P<id>[0-9a-z-]+)/$', 'view_followup', name='view_followup'),
		url(r'^delete_followup/(?P<id>[0-9a-z-]+)/$', 'delete_followup', name='delete_followup'),
		

		# Management Urls
    	url(r'^dss/$', 'dss', name='dss'),
    	url(r'^fetch_drug/$', 'fetch_drug', name='fetch_drug'),
    	url(r'^fetch_dose/$', 'fetch_dose', name='fetch_dose'),
	)

