"""
Naweza-EMR URL Configuration.

Other urls are import
Put here only urls not specific to app
"""
from django.conf.urls import include, url
from django.contrib import admin
from naweza_auth import urls as auth_urls
from django.contrib.auth.views import (
    password_reset_done, password_change, password_change_done)
from django.views.generic import TemplateView
from naweza_forms import urls as forms_urls
from naweza_registry import urls as registry_urls
from naweza_report import urls as report_urls
from naweza_data import urls as data_urls


urlpatterns = [
    url(r'^admin/', include(admin.site.urls), name='admin'),
    url(r'^$', 'naweza.views.home', name='home'),
    # authentication
    url(r'^login/$', 'naweza_auth.views.login', name='login'),
    url(r'^logout/$', 'naweza_auth.views.logout', name='logout'),
    url(r'^signup/$', 'naweza_auth.views.access', name='access'),
    url(r'^password_reset/$', 'naweza_auth.views.password_reset', name='password_reset'),
    url(r'^activate_user/$', 'naweza_auth.views.activate_user', name='activate_user'),

    #registry
    url(r'^registry/', include(registry_urls)),

    #forms
    url(r'^forms/', include(forms_urls)),

    #data
    url(r'^data/', include(data_urls)),

    #data
    url(r'^report/', include(report_urls)),
    ]

handler400 = 'naweza.views.handler_400'
handler404 = 'naweza.views.handler_404'
handler500 = 'naweza.views.handler_500'

admin.site.site_header = 'Naweza Administration'
admin.site.site_title = 'Naweza Administration'
admin.site.index_title = 'Naweza admin'
