"""Main Naweza common views."""
from datetime import datetime, timedelta
from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from naweza.functions import generate_dashboard


def home(request):
    """Some default page for the home page / Dashboard."""

    try:
        dash = generate_dashboard()
        start_date = datetime.now() - timedelta(days=21)
        my_dates, my_cvals, my_kvals, my_dvals = [], [], [], []

        print dash
        total =  dash['total']
        male =  dash['male']
        female =  dash['female'] 
        patient_regs = dash['patient_regs']
        referrals = dash['referrals']
        todayvisits = dash['todayvisits']
        alchohal = dash['alchohal']
        #dvals = dash['dvals']

        # Graph
        for date in range(0, 22, 2):
            end_date = start_date + timedelta(days=date)
            show_date = datetime.strftime(end_date, "%d-%b-%y")
            final_date = str(show_date).replace(' ', '&nbsp;')
            my_dates.append("[%s, '%s']" % (date, final_date))
        for vl in range(1, 22):
            t_date = start_date + timedelta(days=vl)
            s_date = datetime.strftime(t_date, "%d-%b-%y")
            k_count = patient_regs[s_date] if s_date in patient_regs else 0
            # c_count = visit_regs[s_date] if s_date in visit_regs else 0
            my_kvals.append("[%s, %s]" % (vl, k_count))
            # my_cvals.append("[%s, %s]" % (vl, c_count))
        dates = ', '.join(my_dates)
        kvals = ', '.join(my_kvals)

        return render(request, 
            'dashboard.html',
            {
                'total': total,
                'male': male, 
                'female': female,
                'dates': dates, 
                'kvals': kvals,
                'referrals': referrals,
                #'alchohal': alchohal,
                'todayvisits': todayvisits
                #'dvals': dvals
            })

    except Exception, e:  
        raise e     
        print 'Error generating dashboard - (%s)' % str(e)
        return render(request, 'dashboard.html')


def handler_400(request):
    """Some default page for Bad request error page."""
    try:
        return render(request, '400.html', {'status': 400})
    except Exception, e:
        raise e


def handler_404(request):
    """Some default page for the Page not Found."""
    try:
        return render(request, '404.html', {'status': 404})
    except Exception, e:
        raise e


def handler_500(request):
    """Some default page for Server Errors."""
    try:
        return render(request, '500.html', {'status': 500})
    except Exception, e:
        raise e
