import os
import xlrd
import json
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from django.core.cache import cache
from django.db.models import Q, Count
from django.conf import settings
from django.http import HttpResponseRedirect, JsonResponse
from naweza_data.functions import generate_data
from naweza_main.models import SetupGeography, SetupList
from naweza_registry.models import RegOrgUnit, RegPerson, AfyaData
from naweza_forms.models import PatientVisits, PatientObs
from dateutil.relativedelta import relativedelta


def generate_tips(obs):
    try: 
        # get algorithm file
        file_path = os.path.join(settings.DOCUMENT_ROOT, 'naweza_algorithm_v2.xlsx')
        wb = xlrd.open_workbook(file_path)
        sh = wb.sheet_by_index(0)
        actions = []
        jsonData = { 'actions': ['No patient management tips available. Please input some assessment variables or diagnosis'] }

        # Work! Work!
        for rownum in range(1, sh.nrows):
            row_values = sh.row_values(rownum)

            if row_values[1] != '#':
                action = row_values[2]
                for ob in obs:
                    if str(ob) == row_values[1]:
                        if action not in actions:
                            actions.append(str(action))

        if actions:
            jsonData = { 'actions': actions }

        return jsonData

    except Exception, e:
        print 'Error - %s' % str(e)
    else:
        pass
    finally:
        pass

def parse_obs(obs):
    return
    
def generate_age(dob):
    today = date.today()
    fmt = "%d-%b-%Y"
    dob = datetime.strptime(dob, fmt)
    age = today.year - dob.year - ((today.month, today.day) < (dob.month, dob.day)) 
    return age

def generate_color(age, sex, smoker, diabetes, sbp):
    age = int(age)
    sex = 'M' if str(sex.upper())=='SMAL' else 'F'
    smoker = 'YES' if str(smoker.upper())== 'AYES' else 'NO'
    diabetes = 'YES' if str(diabetes.upper())== 'AYES' else 'NO'
    sbp = int(sbp)
    color = 'NO COLOR'

    print '(age %s, sex %s, smoker %s, diabetes %s, sbp %s)' %(age, sex, smoker, diabetes, sbp)

    # Class of >= 70
    if(age >= 70):
        if(sex == "M"):
            if(smoker == "YES"):
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "YELLOW"
                    if(sbp >= 140 and sbp <= 159):
                        color = "ORANGE"
                    if(sbp >= 160 and sbp <= 179):
                        color = "RED"
                    if(sbp >= 180):
                        color = "CRIMSON"
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "YELLOW"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "RED"

            elif(smoker == "NO"):
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "ORANGE"
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "YELLOW"
                    if(sbp >= 160 and sbp <= 179):
                        color = "ORANGE"
                    if(sbp >= 180):
                        color = "CRIMSON"

        elif (sex == 'F'):
            if(smoker == "YES"):
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "YELLOW"
                    if(sbp >= 140 and sbp <= 159):
                        color = "ORANGE"
                    if(sbp >= 160 and sbp <= 179):
                        color = "RED"
                    if(sbp >= 180):
                        color = "CRIMSON"
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "RED"

            elif(smoker == "NO"):
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "ORANGE"
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "ORANGE"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "CRIMSON"

    # Class of 60-69
    if(age >= 60 and age <= 69):
        if(sex == "M"):
            if(smoker == "YES"):
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "YELLOW"
                    if(sbp >= 160 and sbp <= 179):
                        color = "ORANGE"
                    if(sbp >= 180):
                        color = "CRIMSON"
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "RED"

            elif(smoker == "NO"):
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "ORANGE"
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "CRIMSON"

        elif (sex == 'F'):
            if(smoker == "YES"):
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "YELLOW"
                    if(sbp >= 160 and sbp <= 179):
                        color = "ORANGE"
                    if(sbp >= 180):
                        color = "CRIMSON"
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "RED"

            elif(smoker == "NO"):
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "ORANGE"
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "RED"

    # Class of 50-59
    if(age >= 50 and age <= 59):
        if(sex == "M"):
            if(smoker == "YES"):
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "ORANGE"
                    if(sbp >= 180):
                        color = "CRIMSON"
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "RED"

            elif(smoker == "NO"):
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "RED"
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "RED"

        elif (sex == 'F'):
            if(smoker == "YES"):
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "CRIMSON"
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "RED"

            elif(smoker == "NO"):
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "YELLOW"
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "RED"

    # Class of 40-49
    if(age >= 40 and age <= 49):
        if(sex == "M"):
            if(smoker == "YES"):
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "ORANGE"
                    if(sbp >= 180):
                        color = "CRIMSON"
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "RED"

            elif(smoker == "NO"):
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "YELLOW"
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "RED"

        elif (sex == 'F'):
            if(smoker == "YES"):
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "ORANGE"
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "ORANGE"

            elif(smoker == "NO"):
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "YELLOW"
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "ORANGE"
    print 'color (%s)' % color
    return color

def generate_dashboard():
    print 'Preparing the afyachat dashboard . . .'
    try:
        dash = {}

        pt_visits = PatientVisits.objects.filter(is_void=False)
        pt_data = AfyaData.objects.filter(active=True)
        obs_data = PatientObs.objects.filter(is_void=False)
        
        referrals_obs = obs_data.filter(obs_name ='referral_reason').values('visit_id').distinct()
        referrals = referrals_obs.count()
        
        # print obs_data.query
        today = date.today()
        todayvisits = pt_visits.filter(visit_date=today).count()
        print todayvisits

        # Dashboard
        total = pt_data.count()
        male = pt_data.exclude(sex='F').count()
        female = pt_data.exclude(sex='M').count()  

        # Graph
        pregs = pt_visits.values('created_at').annotate(unit_count=Count('created_at'))
        patient_regs, visit_regs, case_regs = {}, {}, {}
        for preg in pregs:
            the_date = preg['created_at']
            pdate = the_date.strftime('%d-%b-%y')
            patient_regs[str(pdate)] = preg['unit_count']

        """
        # Donuts
        my_dvals, cnt = [], 0
        colors = ['#e41a1c', '#377eb8', '#4daf4a', '#984ea3', '#ff7f00',
                  '#ffff33']

        #Chart Variables
        denominator = pt_visits.count()
        others = 0
        tobacco_use = 0
        alcohal_consumption = 0

        obs = PatientObs.objects.all();
        obs_array = obs.filter(obs_name='tobacco_use', obs_value='YES', is_void=False).values('obs_name').annotate(total=Count('obs_name')).order_by('obs_name')

        print 'obs_array %s' %obs_array

        #print 'tobacco : %s' %tobacco
        # filter(diagnosis_result='AYES', is_void=False).values('diagnosis_type').annotate(total=Count('diagnosis_type')).order_by('diagnosis_type')
        indicator_other = 0
        for obs in obs_array:
            obs_name = obs['obs_name']
            obs_value = obs['total']
            if cnt > 4:
                indicator_other += obs_value
            else:
                my_data = '{label: "%s", data: %s, color: "%s"}' % (
                    obs_name, obs_value, colors[cnt])
                my_dvals.append(my_data)
            cnt += 1


        dvals = ', '.join(my_dvals)

        print dvals
        """

        dash['total'] = total
        dash['male'] = male
        dash['female'] = female
        dash['patient_regs'] = patient_regs
        dash['referrals'] = referrals
        #dash['dvals'] = dvals
        
    except Exception, e:
        print 'Error preparing the naweza dashboard - %s' % str(e)
        pass
    else:
        return dash
