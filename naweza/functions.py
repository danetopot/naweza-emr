# coding=utf-8
import os
import xlrd
import json
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from django.core.cache import cache
from django.db.models import Q, Count
from django.conf import settings
from django.http import HttpResponseRedirect, JsonResponse
from naweza_data.functions import generate_data
from naweza_main.models import SetupGeography, SetupList
from naweza_registry.models import RegOrgUnit, RegPerson, AfyaData
from naweza_forms.models import PatientVisits, PatientObs
from dateutil.relativedelta import relativedelta


def generate_tips(obs):
    try: 
        print 'obs@functions.py : %s' %obs
        # get algorithm file
        file_path = os.path.join(settings.DOCUMENT_ROOT, 'naweza_algorithm_v2.xlsx')
        wb = xlrd.open_workbook(file_path)
        sh = wb.sheet_by_index(0)
        actions = []
        jsonData = { 'actions': ['No patient management tips available. Please input some assessment variables or diagnosis'] }

        # Work! Work!
        for rownum in range(1, sh.nrows):
            row_values = sh.row_values(rownum)

            if row_values[1] != '#':
                action = (row_values[3]).encode('utf-8').strip()
                for ob in obs:
                    if str(ob) == row_values[1]:
                        if action not in actions:
                            actions.append(str(action))

        if actions:
            jsonData = { 'actions': actions }

        return jsonData

    except Exception, e:
        print 'Error - %s' % str(e)
    else:
        pass
    finally:
        pass

def parse_obs(obs):
    return

def generate_risk(color):
    risk = None

    if color == 'GREEN':
        risk='Risk<10'
    elif color == 'YELLOW':
        risk = 'Risk>10 and <20'
    elif color == 'ORANGE':
        risk = 'Risk>20 and <30'
    elif (color == 'RED' or color=='CRIMSON'):
        risk = 'Risk>30'  
    return risk
    
def generate_age(dob):
    today = date.today()
    fmt = "%d-%b-%Y"
    dob = datetime.strptime(dob, fmt)
    age = today.year - dob.year - ((today.month, today.day) < (dob.month, dob.day)) 
    return age

def generate_color(age, sex, smoker, diabetes, sbp):
    """
    age = int(age)
    sex = 'M' if str(sex.upper())=='SMAL' else 'F'
    smoker = 'YES' if str(smoker.upper())== 'AYES' else 'NO'
    diabetes = 'YES' if str(diabetes.upper())== 'AYES' else 'NO'
    sbp = int(sbp)
    """
    # print age, sex, smoker, diabetes, sbp
    color = 'NO COLOR'

    # Class of >= 70
    if(age >= 70):
        if(sex == "M"):
            if(smoker == "YES"):
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "YELLOW"
                    if(sbp >= 140 and sbp <= 159):
                        color = "ORANGE"
                    if(sbp >= 160 and sbp <= 179):
                        color = "RED"
                    if(sbp >= 180):
                        color = "CRIMSON"
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "YELLOW"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "RED"

            elif(smoker == "NO"):
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "ORANGE"
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "YELLOW"
                    if(sbp >= 160 and sbp <= 179):
                        color = "ORANGE"
                    if(sbp >= 180):
                        color = "CRIMSON"

        elif (sex == 'F'):
            if(smoker == "YES"):
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "YELLOW"
                    if(sbp >= 140 and sbp <= 159):
                        color = "ORANGE"
                    if(sbp >= 160 and sbp <= 179):
                        color = "RED"
                    if(sbp >= 180):
                        color = "CRIMSON"
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "RED"

            elif(smoker == "NO"):
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "ORANGE"
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "ORANGE"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "CRIMSON"

    # Class of 60-69
    if(age >= 60 and age <= 69):
        if(sex == "M"):
            if(smoker == "YES"):
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "YELLOW"
                    if(sbp >= 160 and sbp <= 179):
                        color = "ORANGE"
                    if(sbp >= 180):
                        color = "CRIMSON"
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "RED"

            elif(smoker == "NO"):
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "ORANGE"
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "CRIMSON"

        elif (sex == 'F'):
            if(smoker == "YES"):
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "YELLOW"
                    if(sbp >= 160 and sbp <= 179):
                        color = "ORANGE"
                    if(sbp >= 180):
                        color = "CRIMSON"
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "RED"

            elif(smoker == "NO"):
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "ORANGE"
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "RED"

    # Class of 50-59
    if(age >= 50 and age <= 59):
        if(sex == "M"):
            if(smoker == "YES"):
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "ORANGE"
                    if(sbp >= 180):
                        color = "CRIMSON"
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "RED"

            elif(smoker == "NO"):
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "RED"
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "RED"

        elif (sex == 'F'):
            if(smoker == "YES"):
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "CRIMSON"
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "RED"

            elif(smoker == "NO"):
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "YELLOW"
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "RED"

    # Class of 40-49
    if(age >= 40 and age <= 49):
        if(sex == "M"):
            if(smoker == "YES"):
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "ORANGE"
                    if(sbp >= 180):
                        color = "CRIMSON"
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "YELLOW"
                    if(sbp >= 180):
                        color = "RED"

            elif(smoker == "NO"):
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "YELLOW"
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "RED"

        elif (sex == 'F'):
            if(smoker == "YES"):
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "ORANGE"
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "ORANGE"

            elif(smoker == "NO"):
                if(diabetes == "NO"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "YELLOW"
                if(diabetes == "YES"):
                    if(sbp > 0 and sbp <= 139):
                        color = "GREEN"
                    if(sbp >= 140 and sbp <= 159):
                        color = "GREEN"
                    if(sbp >= 160 and sbp <= 179):
                        color = "GREEN"
                    if(sbp >= 180):
                        color = "ORANGE"
    # print 'color (%s)' % color
    return color

def generate_dashboard():
    print 'Preparing the afyachat dashboard . . .'
    try:
        dash = {}

        pt_visits = PatientVisits.objects.filter(is_void=False)
        pt_data = AfyaData.objects.filter(active=True)
        obs_data = PatientObs.objects.filter(is_void=False)
        
        referrals_obs = obs_data.filter(obs_name ='referral_reason').values('visit_id').distinct()
        referrals = referrals_obs.count()

        alchohal_obs = obs_data.filter(obs_name ='alcohal_consumption')
        alchohal = alchohal_obs.count()
        
        # print obs_data.query
        today = date.today()
        todayvisits = pt_visits.filter(visit_date=today).count()
        print todayvisits

        # Dashboard
        total = pt_data.count()
        male = pt_data.exclude(sex='F').count()
        female = pt_data.exclude(sex='M').count()  

        # Graph
        pregs = pt_visits.values('created_at').annotate(unit_count=Count('created_at'))
        patient_regs, visit_regs, case_regs = {}, {}, {}
        for preg in pregs:
            the_date = preg['created_at']
            pdate = the_date.strftime('%d-%b-%y')
            patient_regs[str(pdate)] = preg['unit_count']

       

        dash['total'] = total
        dash['male'] = male
        dash['female'] = female
        dash['patient_regs'] = patient_regs
        dash['referrals'] = referrals
        dash['todayvisits'] = todayvisits
        dash['alchohal'] = alchohal
        #dash['dvals'] = dvals
        
    except Exception, e:
        print 'Error preparing the naweza dashboard - %s' % str(e)
        pass
    else:
        return dash
