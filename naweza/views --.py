"""Main Naweza common views."""
from datetime import datetime, timedelta
from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from naweza.functions import generate_dashboard


def home(request):
    """Some default page for the home page / Dashboard."""

    try:
        dash = generate_dashboard()

        print dash
        total =  dash['total']
        male =  dash['male']
        female =  dash['female']
        refferal = dash['refferal']
        patient_regs = dash['patient_regs']
        results =  dash['results']

        my_dates, my_cvals, my_kvals = [], [], []
        my_dvals, count = [], 0

        start_date = datetime.now() - timedelta(days=21)
        for date in range(0, 22, 2):
            end_date = start_date + timedelta(days=date)
            show_date = datetime.strftime(end_date, "%d-%b-%y")
            final_date = str(show_date).replace(' ', '&nbsp;')
            my_dates.append("[%s, '%s']" % (date, final_date))
        for vl in range(1, 22):
            t_date = start_date + timedelta(days=vl)
            s_date = datetime.strftime(t_date, "%d-%b-%y")
            k_count = patient_regs[s_date] if s_date in patient_regs else 0
            c_count = patient_regs[s_date] if s_date in patient_regs else 0
            my_cvals.append("[%s, %s]" % (vl, c_count))
            my_kvals.append("[%s, %s]" % (vl, k_count))
        dates = ', '.join(my_dates)
        kvals = ', '.join(my_kvals)
        cvals = ', '.join(my_cvals)

        if results:
            for test_result in results:
                result_name = ''
                result_id = test_result['diagnosis_type']
                result_data = test_result['unit_count']

                if result_id == 'DGBA':
                    result_name = 'Bronchial Asthma'
                    colors = '#008744'
                elif result_id == 'DGCS':
                    result_name = 'Cancer (Suspected)'
                    colors = 'f1ff7d'
                elif result_id == 'DGDM':
                    result_name = 'Diabetes Mellitus'
                    colors = '#ffa700'
                elif result_id == 'DGST':
                    result_name = 'Stroke'
                    colors = '#ff0000'
                elif result_id == 'COPD':
                    result_name = 'Chronic Obstructive Pulmonary Disease (COPD))'
                    colors = '#0057e7'
                elif result_id == 'DAMI':
                    result_name = 'Acute myocardial infarction'
                    colors = '#ff7f00'
                elif result_id == 'DAHD':
                    result_name = 'ANgina'
                    colors = '#4daf4a'                    
                else:
                    result_name = 'Others'
                    colors = '#dc143c'

                my_data = '{label: "%s", data: %s, color: "%s"}' % (result_name, result_data, colors)
                my_dvals.append(my_data)
        else:
            my_dvals.append('{label: "No data", data: 0, color: "#fd8d3c"}')
        dvals = ', '.join(my_dvals)


        return render(request, 
            'dashboard.html',
            {
                'total': total,
                'male': male, 'female': female,
                'refferal':refferal,
                'dvals': dvals, 'dates': dates, 
                'kvals': kvals , 'cvals': cvals,
            })

    except Exception, e:  
        raise e     
        print 'Error generating dashboard - (%s)' % str(e)
        return render(request, 'dashboard.html')


def handler_400(request):
    """Some default page for Bad request error page."""
    try:
        return render(request, '400.html', {'status': 400})
    except Exception, e:
        raise e


def handler_404(request):
    """Some default page for the Page not Found."""
    try:
        return render(request, '404.html', {'status': 404})
    except Exception, e:
        raise e


def handler_500(request):
    """Some default page for Server Errors."""
    try:
        return render(request, '500.html', {'status': 500})
    except Exception, e:
        raise e
