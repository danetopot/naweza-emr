"""Main configurations information."""
from django.apps import AppConfig


class AuthAppConfig(AppConfig):
    """Details for authentication module."""

    name = 'naweza_auth'
    verbose_name = 'User Management'


class MainAppConfig(AppConfig):
    """Details for common application module."""

    name = 'naweza_main'
    verbose_name = 'System Lookups and Settings'


class RegAppConfig(AppConfig):
    """Details for Registry modules."""

    name = 'naweza_registry'
    verbose_name = 'Registry Management'


class AccessAppConfig(AppConfig):
    """Details for Registry modules."""

    name = 'naweza_access'
    verbose_name = 'Access Management'

class AccessAppConfig(AppConfig):
    """Details for Registry modules."""

    name = 'naweza_forms'
    verbose_name = 'Forms Management'
