var handleBootstrapWizardsValidation = function() {
    "use strict";
    var error_msg = 'Please provide required ';
    $("#submit_followup").removeClass( "btn-primary" ).attr("disabled", "disabled");
    // $("#mysubmit2").removeClass( "btn-primary" ).attr("disabled", "disabled");
    
    $("#wizard_persons").bwizard({ validating: function (e, ui) { 
            if (ui.index == 0) {
                // step-1 validation
                if (false === $('form[name="form-wizard"]').parsley().validate('group0')) {
                    $(".alert").show();
                    $('#invalid-form-message').html(error_msg + 'demographic details.');
                    return false;
                }
            }else if (ui.index == 1) {
                // step-2 validation 
                $("#mysubmit").addClass( "btn-primary" ).removeAttr("disabled");
                  if (false === $('form[name="form-wizard"]').parsley().validate('group1')) {
                    $(".alert").show();
                    $('#invalid-form-message').html(error_msg + 'indicator details.');
                    return false;
                }
            }else if (ui.index == 2) {
                // step-3 validation  
                  if (false === $('form[name="form-wizard"]').parsley().validate('group2')) {
                    $(".alert").show();
                    $('#invalid-form-message').html(error_msg + 'contact details.');
                    return false;
                }
            }
        } 
    });

    $("#wizard_clinics").bwizard({ validating: function (e, ui) { 
            if (ui.index == 0) {
                // step-1 validation 
                $("#mysubmit2").addClass( "btn-primary" ).removeAttr("disabled");
                if (false === $('form[name="form-wizard"]').parsley().validate('group0')) {
                    return false;
                }
            }else if (ui.index == 1) {
                // step-2 validation
                  if (false === $('form[name="form-wizard"]').parsley().validate('group1')) {
                    return false;
                }
            }
        } 
    });

    $("#wizard_new_clinic_form").bwizard({ validating: function (e, ui) {
        if (ui.index == 0) {
                // step-1 validation
                if (false === $('form[name="form-wizard"]').parsley().validate('group0')) {
                    $(".alert").show();
                    $('#invalid-form-message').html(error_msg + 'history details.');
                    return false;
                    // return true;
                }
                else{
                    $(".alert").hide();
                }
            } else if (ui.index == 1) {
                // step-2 validation
                var glucose_level = $('#glucose_level').val();
                if(glucose_level=='YES' || glucose_level > 7)
                {
                    $('#blood_sugar').attr('data-parsley-required', 'true');
                } else{
                    $('#blood_sugar').attr('data-parsley-required', 'false');
                    $('#blood_sugar').val('');
                    $(".alert").hide();
                }

                if (false === $('form[name="form-wizard"]').parsley().validate('group1')) {
                    $(".alert").show();
                    $('#invalid-form-message').html(error_msg + 'assessment details.');
                    
                    return false;
                    //return true;
                }else{
                    $(".alert").hide();
                    $('#divBtnSmartTips').show();

                }
            }else if (ui.index == 2) {
                // step-3 validation                
                $("#submit_followup").addClass( "btn-primary" ).removeAttr("disabled");                 
                  if (false === $('form[name="form-wizard"]').parsley().validate('group2')) {
                    $(".alert").show();
                    $('#invalid-form-message').html(error_msg + 'medication details.');
                    return false;
                    //return true;
                }else{
                    $(".alert").hide();
                }
            }else if (ui.index == 3) {
                // step-4 validation
                 
                  if (false === $('form[name="form-wizard"]').parsley().validate('group3')) {
                    $(".alert").show();
                    $('#invalid-form-message').html(error_msg + 'referral details.');
                    return false;
                    //return true;
                }else{
                    $(".alert").hide();
                }
            } 
        }
    });

    /*
    $("#wizard_new_clinic_form").bwizard({ validating: function (e, ui) { 
            if (ui.index == 0) {
                // step-1 validation

                var rows = $('#diagnosis_manager_table tr').length;
                if (rows == 2)
                { 
                    $(".alert").show();
                    $('.invalid-form-message').html(error_msg + 'diagnosis details.');
                    return false;
                }
                else if (false === $('form[name="form-wizard"]').parsley().validate('group0')) {
                    return false;
                }
            }else if (ui.index == 1) {
                // step-2 validation
                
                var rows = $('#investigations_manager_table tr').length;
                if (rows == 2)
                { 
                    $(".alert").show();
                    $('.invalid-form-message').html(error_msg + 'investigations details.');
                    return false;
                }
                getActions(); 
            }
            else if (ui.index == 2) {
                // step-2 validation
                
                var rows = $('#medications_manager_table tr').length;
                if (rows == 2)
                { 
                    $(".alert").show();
                    $('.invalid-form-message').html(error_msg + 'medications details.');
                    return false;
                }
            }
            else if (ui.index == 3) {
                // step-2 validation
                if (false === $('form[name="form-wizard"]').parsley().validate('group3')) {
                    return false;
                }
            }
        } 
    });
    */
};

var FormWizardValidation = function () {
    "use strict";
    return {
        //main function
        init: function () {
            handleBootstrapWizardsValidation();
        }
    };
}();