from __future__ import absolute_import

from celery import Celery

from .functions import send_sms

app = Celery('tasks', backend='rpc://', broker='pyamqp://')

@app.task
def test():
	print 'Calling SMS API . . . '
	return

@app.task
def api_call():
	send_sms()
	return