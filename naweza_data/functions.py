# coding=utf-8
from django.db.models import Count
import datetime
import psycopg2
import csv
import sys
import os
import stat
import shutil
import random
import re
from naweza_forms.models import PatientVisits, PatientObs, PatientMedications
from naweza_main.functions import translate


# Get Data
def generate_data():
    try:
        obs_dict = PatientObs.objects.values('obs_name', 'obs_value')
        obs_list = [entry for entry in obs_dict]
        return obs_list
    except Exception, e:
        print 'Error in generate_csv - %s' % str(e)
    else:
        pass
    finally:
        pass
    return True


def generate_data():
    try:
        obs_dict = PatientObs.objects.values('obs_name', 'obs_value')
        obs_list = [entry for entry in obs_dict]
        return obs_list
    except Exception, e:
        print 'Error in generate_csv - %s' % str(e)
    else:
        pass
    finally:
        pass
    return True

# Generate CSV


def generate_csv():
    csv_header = []
    dateOfToday = datetime.datetime.now().date()

    randomnumber = random.randint(1000, 9999)
    outfile = "C:\\NAWEZA\\naweza_data_" + str(dateOfToday) + \
        "_" + str(randomnumber) + ".csv"
    try:
        data = generate_data()

        # sql = "SELECT v.patient_id , f.fname, f.lname, f.age, o.obs_name, o.obs_value, v.visit_date, v.visit_no, v.created_at FROM  patient_obs o INNER JOIN patient_visits v ON o.visit_id = v.visit INNER JOIN afyachat_afyadata f ON f.id = v.patient_id"  + \
        conn = psycopg2.connect(database="naweza", user="postgres",
                                                        password="postgres", host="127.0.0.1", port="5432")
        print 'Connection to database success ..'

        cursor = conn.cursor()
        resultset = cursor.execute(
                """
			SELECT 'SELECT * FROM   crosstab(
			$ct$SELECT u.attnum, t.rn, u.val FROM  (SELECT row_number() OVER () AS rn, * FROM '
			|| attrelid::regclass || ') t , unnest(ARRAY[' || string_agg(quote_ident(attname)
			|| '::text', ',') || ']) WITH ORDINALITY u(val, attnum) ORDER  BY 1, 2$ct$) t (attnum bigint, '
			|| (SELECT string_agg('r'|| rn ||' text', ', ') FROM  (SELECT row_number() OVER () AS rn FROM patient_obs) t)
			|| ')' AS sql FROM   pg_attribute
			WHERE  attrelid = 'patient_obs'::regclass
			AND    attnum > 0 AND    NOT attisdropped
			GROUP  BY attrelid;
			""")
        rows = cursor.fetchall()

        # get inner sql
        inner_Sql = None
        for row in rows:
            inner_sql = str(row[0])

        inner_sql = re.sub(r'[^\x00-\x7F]+', ' ', inner_sql)
        outer_sql = "COPY (" + inner_sql + ") TO '" + \
            outfile + "'With  DELIMITER ',' CSV HEADER"
        cursor.execute(outer_sql)
        cursor.close()

    except Exception, e:
        print 'Error in generate_csv - %s' % str(e)
    else:
        pass
    finally:
        pass
    return True

# Generate CSV


def generate_agg_csv():
    csv_header = []
    dateOfToday = datetime.datetime.now().date()

    randomnumber = random.randint(1000, 9999)
    outfile = "C:\\NAWEZA\\naweza_data_" + str(dateOfToday) + \
        "_" + str(randomnumber) + ".csv"

    try:
        data = generate_data()

        # sql = "SELECT v.patient_id , f.fname, f.lname, f.age, o.obs_name, o.obs_value, v.visit_date, v.visit_no, v.created_at FROM  patient_obs o INNER JOIN patient_visits v ON o.visit_id = v.visit INNER JOIN afyachat_afyadata f ON f.id = v.patient_id"  + \
        conn = psycopg2.connect(database="naweza", user="postgres",
                                                        password="postgres", host="127.0.0.1", port="5432")
        print 'Connection to database success ..'

        cursor = conn.cursor()
        resultset = cursor.execute(
                """
			select * from afyachat_afyadata T1
			inner join patient_obs T2 ON T2.patient_id=T1.id
			inner join patient_medications T3 ON T3.patient_id=T1.id
			""")
        rows = cursor.fetchall()

        header = []
        for column in cursor.description:
            header.append(str(column[0]))

        f = open(outfile, 'w')
        with f:
            writer = csv.writer(f)
            writer.writerow(header)
        cursor.close()

    except Exception, e:
        print 'Error in generate_csv - %s' % str(e)
    else:
        pass
    finally:
        pass
    return True


def generate_obs_csv():
    csv_header = []
    dateOfToday = datetime.datetime.now().date()

    randomnumber = random.randint(1000, 9999)
    csvfile = "C:\\NAWEZA\\naweza_emr_data_" + str(dateOfToday) + \
        "_" + str(randomnumber) + ".csv"

    try:
        conn = psycopg2.connect(database="naweza", user="postgres",
                                                        password="postgres", host="127.0.0.1", port="5432")
        print 'Connection to database success ..'

        cursor = conn.cursor()
        visit_sql = (
                """
			select T1.*, T2.visit, T2.visit_date, T2.visit_notes from afyachat_afyadata T1
			inner join patient_visits T2 ON T2.patient_id=T1.id
            -- where T2.visit='b043534f-4560-11e9-b9e9-f0189870e25b'
			""")
        resultset = cursor.execute(visit_sql)
        rows = cursor.fetchall()

        header = [
        'id',
        'fname',
        'lname',
        'age',
        'sex',
        'glucose',
        'smoker',
        'diabetes',
        'phone_no',
        'sbp(screening)',
        'color',
        'datecreated',
        'reporter',
        'active',
        'visit',
        'visit_date',
        'visit_notes',  
        'past_history',
        'past_history_other',
        'current_symptoms',
        'current_symptoms_other',
        'tobacco_use',
        'physical_activity',
        'family_history',
        'activity_status',
        'occupation',
        'current_medication',
        'alcohal_consumption',
        'alcohal_amount',
        'alcohal_frequency',
        'waist_circumference',
        'sbp',
        'dbp',
        'oedema',
        'heaving',
        'displacement',
        'ausculate_heart',
        'ausculate_heart_describe',
        'ausculate_lungs',
        'ausculate_lungs_describe',
        'abdomen_exam',
        'abdomen_exam_describe',
        'dm_exam',
        'dm_exam_describe',
        'blood_sugar_type',
        'blood_sugar',
        'urine_ketones',
        'urine_protein',
        'cholesterol',
        'assessment_comments',
        'referral_reason',
        'referral_reason_other',
        'medication'
        ]

        # Assessment Data
        obs = PatientObs.objects.values('patient_id','visit_id','obs_name','obs_value')
        meds = PatientMedications.objects.values('patient_id','visit_id','medication_name','medication_dosage')
        
        with open(csvfile, 'wb') as outcsv:
            writer = csv.writer(outcsv)
            writer.writerow(header)
            for row in rows:
                data = []
                new_row = []
                records = obs.filter(visit_id=row[14])
                medical = meds.filter(visit_id=row[14])
                
                current_symptoms,current_symptoms_other, str_current_symptoms = [], None, None
                past_history,past_history_other, str_past_history = [], None, None
                referral_reason,referral_reason_other, str_referral_reason = [], None, None
                occupation, str_occupation = [], None
                current_medication, str_current_medication = [], None

                for record in records:
                    key = record['obs_name']
                    value = str(translate(record['obs_value'])) 

                    if(key=='past_history'):
                        past_history.append(value)
                        str_past_history = ', '.join(past_history); 
                    if(key=='past_history_other'):
                        past_history_other = value
                    if(key=='current_symptoms'):
                        current_symptoms.append(value)
                        str_current_symptoms = ', '.join(current_symptoms); 
                    if(key=='current_symptoms_other'):
                        current_symptoms_other = value
                    if(key=='tobacco_use'):
                        tobacco_use = value
                    if(key=='physical_activity'):
                        physical_activity = value
                    if(key=='family_history'):
                        family_history = value
                    if(key=='activity_status'):
                        activity_status = value
                    if(key=='occupation'):
                        occupation.append(value)
                        str_occupation = ', '.join(occupation); 
                    if(key=='current_medication'):
                        current_medication.append(value)
                        str_current_medication = ', '.join(current_medication); 
                    if(key=='alcohal_consumption'):
                        alcohal_consumption = value
                    if(key=='alcohal_amount'):
                        alcohal_amount = value
                    if(key=='alcohal_frequency'):
                        alcohal_frequency = value
                    if(key=='waist_circumference'):
                        waist_circumference = value
                    if(key=='sbp'):
                        sbp = value
                    if(key=='dbp'):
                        dbp = value
                    if(key=='oedema'):
                        oedema = value
                    if(key=='heaving'):
                        heaving = value
                    if(key=='displacement'):
                        displacement = value
                    if(key=='ausculate_heart'):
                        ausculate_heart = value
                    if(key=='ausculate_heart_describe'):
                        ausculate_heart_describe = value
                    if(key=='ausculate_lungs'):
                        ausculate_lungs = value
                    if(key=='ausculate_lungs_describe'):
                        ausculate_lungs_describe = value
                    if(key=='abdomen_exam'):
                        abdomen_exam = value
                    if(key=='abdomen_exam_describe'):
                        abdomen_exam_describe = value
                    if(key=='dm_exam'):
                        dm_exam = value
                    if(key=='dm_exam_describe'):
                        dm_exam_describe = value
                    if(key=='blood_sugar_type'):
                        blood_sugar_type = value
                    if(key=='blood_sugar'):
                        blood_sugar = value
                    if(key=='urine_ketones'):
                        urine_ketones = value
                    if(key=='urine_protein'):
                        urine_protein = value
                    if(key=='cholesterol'):
                        cholesterol = value
                    if(key=='assessment_comments'):
                        assessment_comments = value
                    if(key=='referral_reason'):
                        referral_reason.append(value)
                        str_referral_reason = ', '.join(referral_reason); 
                    if(key=='referral_reason_other'):
                        referral_reason_other = value

                data.append(str_past_history)
                data.append(past_history_other)
                data.append(str_current_symptoms)
                data.append(current_symptoms_other)
                data.append(tobacco_use)
                data.append(physical_activity)
                data.append(family_history)
                data.append(activity_status)
                data.append(str_occupation)
                data.append(str_current_medication)
                data.append(alcohal_consumption)
                data.append(alcohal_amount)
                data.append(alcohal_frequency)
                data.append(waist_circumference)
                data.append(sbp)
                data.append(dbp)
                data.append(oedema)
                data.append(heaving)
                data.append(displacement)
                data.append(ausculate_heart)
                data.append(ausculate_heart_describe)
                data.append(ausculate_lungs)
                data.append(ausculate_lungs_describe)
                data.append(abdomen_exam)
                data.append(abdomen_exam_describe)
                data.append(dm_exam)
                data.append(dm_exam_describe)
                data.append(blood_sugar_type)
                data.append(blood_sugar)
                data.append(urine_ketones)
                data.append(urine_protein)
                data.append(cholesterol)
                data.append(assessment_comments)
                data.append(str_referral_reason)
                data.append(referral_reason_other)

                medication, str_medication = [], None
                for med in medical:
                    key = str(translate(med['medication_name']))
                    value = str(translate(med['medication_dosage'])) 
                    medication.append(key+' '+value)
                    str_medication = ', '.join(medication);
                data.append(str_medication)



                new_row =list(row) + data
                
                writer.writerow(new_row)

        cursor.close()
    except Exception, e:
        print 'Error in generate_csv - %s' % str(e)
    else:
        pass
    finally:
        pass
    return True


def generate_meds_csv():
    csv_header = []
    dateOfToday = datetime.datetime.now().date()

    randomnumber = random.randint(1000, 9999)
    outfile = "C:\\NAWEZA\\naweza_medications_data_" + str(dateOfToday) + \
        "_" + str(randomnumber) + ".csv"

    try:
        conn = psycopg2.connect(database="naweza", user="postgres",
                                                        password="postgres", host="127.0.0.1", port="5432")
        print 'Connection to database success ..'

        cursor = conn.cursor()
        inner_sql = (
                """
			select * from afyachat_afyadata T1
			inner join patient_medications T3 ON T3.patient_id=T1.id
			""")
        rows = cursor.fetchall()
        outer_sql = "COPY (" + inner_sql + ") TO '" + \
            outfile + "'With  DELIMITER ',' CSV HEADER"
        cursor.execute(outer_sql)
        cursor.close()

    except Exception, e:
        print 'Error in generate_csv - %s' % str(e)
    else:
        pass
    finally:
        pass
    return True
