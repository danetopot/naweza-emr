# forms.py

from django import forms
from django.utils.translation import ugettext_lazy as _

class UploadForm(forms.Form):
  download_path = forms.CharField(widget=forms.TextInput(
    attrs={'placeholder': _('/home/administrator/Desktop/DATA/'),
         'class': 'form-control',
         'id': 'download_path',
         'readonly': 'true'
         }))

class DownloadForm(forms.Form):
  download_path = forms.CharField(widget=forms.TextInput(
    attrs={'placeholder': _('/home/administrator/Desktop/DATA/'),
         'class': 'form-control',
         'id': 'download_path',
         'readonly': 'true'
         }))