import simplejson as json
import uuid
import json
from django.shortcuts import render
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, JsonResponse
from naweza_data.forms import UploadForm, DownloadForm
from naweza_data.functions import generate_agg_csv, generate_obs_csv, generate_meds_csv, generate_csv

# Create your views here.

def upload(request):
	try:
		msg = 'Upload Success.Find your data on the Desktop in a, gen directory named DATA.DO NOT DELETE THIS DIRECTORY.'
		messages.add_message(request, messages.INFO, msg)
		form = UploadForm()
		return render(request, 'data/upload.html', {'status': 200, 'form': form})
	except Exception as e:
		raise e
	

def download(request):
	try:
		generate_obs_csv()
		msg = 'Download Success.Find your data on the Desktop in a directory named DATA.DO NOT DELETE THIS DIRECTORY.'
		messages.add_message(request, messages.INFO, msg)
		form = UploadForm()
		return render(request, 'data/download.html', {'status': 200, 'form': form})
	except Exception as e:
		raise e
