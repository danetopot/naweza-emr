import simplejson as json
import uuid
import json
from django.shortcuts import render
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, JsonResponse


# Create your views here.

def report_error(request):
	"""Some default page for the Page not Found."""
	try:
		return render(request, '400.html', {'status': 404})
	except Exception, e:
		raise e