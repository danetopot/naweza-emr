"""URLs for authentication module."""
from django.conf.urls import patterns, url

# This should contain urls related to auth app ONLY
urlpatterns = patterns('naweza_report.views',
		url(r'^report_error/$', 'report_error', name='report_error')
	)

