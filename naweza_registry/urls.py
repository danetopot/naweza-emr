"""URLs for authentication module."""
from django.conf.urls import patterns, url

# This should contain urls related to auth app ONLY
urlpatterns = patterns('naweza_registry.views',
		# person
		url(r'^person_search/$', 'person_search', name='person_search'),
		url(r'^visits_search/$', 'visits_search', name='visits_search'),
		url(r'^new_person/$', 'new_person', name='new_person'),
		url(r'^view_person/(?P<uuid>[0-9A-Fa-f-]+)/$', 'view_person', name='view_person'),
		url(r'^edit_person/(?P<uuid>[0-9A-Fa-f-]+)/$', 'edit_person', name='edit_person'),
		url(r'^delete_person/(?P<uuid>[0-9A-Fa-f-]+)/$', 'delete_person', name='delete_person'),

		# patient
		url(r'^view_patient/(?P<id>\d+)/$', 'view_patient', name='view_patient'),
		url(r'^edit_patient/(?P<id>\d+)/$', 'edit_patient', name='edit_patient'),
		url(r'^delete_patient/(?P<id>\d+)/$', 'delete_patient', name='delete_patient'),
		url(r'^exit_patient/(?P<id>\d+)/$', 'exit_patient', name='exit_patient'),

		# clinic
		url(r'^clinic_search/$', 'clinic_search', name='clinic_search'),
		url(r'^new_clinic/$', 'new_clinic', name='new_clinic'),
		url(r'^edit_clinic/$', 'edit_clinic', name='edit_clinic'),
		url(r'^view_clinic/$', 'view_clinic', name='view_clinic'),
		url(r'^delete_clinic/$', 'delete_clinic', name='delete_clinic'),
	)

