"""Registry common functions."""
import uuid
import json
from datetime import datetime, timedelta
from django.utils import timezone
from django.shortcuts import get_object_or_404
from naweza_main.models import SetupGeography

def dashboard():
    """Method to get dashboard totals."""
    try:
        print 'get dashboard'        
    except Exception:
        pass
    else:
        return dash


def get_geo_selected(results, datas, extras, filters=False):
    """Get specific Geography based on existing ids."""
    wards = []
    all_list = get_all_geo_list(filters)
    results['wards'] = datas
    area_ids = map(int, datas)
    selected_ids = map(int, extras)
    # compare
    for geo_list in all_list:
        parent_area_id = geo_list['parent_area_id']
        area_id = geo_list['area_id']
        area_name = geo_list['area_name']
        if parent_area_id in area_ids:
            final_list = '%s,%s' % (area_id, area_name)
            wards.append(final_list)
        # attach already selected
        if area_id in selected_ids:
            extra_list = '%s,%s' % (area_id, area_name)
            wards.append(extra_list)
    unique_wards = list(set(wards))
    results['wards'] = unique_wards
    return results


def get_all_geo_list(filters=False):
    """Get all Geo Locations."""
    try:
        geo_lists = SetupGeography.objects.all()
        geo_lists = geo_lists.filter(is_void=False)
        if filters:
            all_geos = get_user_geos(filters)
            if all_geos:
                subcounty_list = list(all_geos['sub_counties'])
                all_ids = subcounty_list + list(all_geos['wards'])
                geo_lists = geo_lists.filter(area_id__in=all_ids)
        geo_lists = geo_lists.values(
            'area_id', 'area_type_id', 'area_name', 'parent_area_id')
        # .exclude(area_type_id='GPRV')
    except Exception, e:
        raise e
    else:
        return geo_lists


def get_geo_list(geo_lists, geo_filter, add_select=False, user_filter=[]):
    """Get specific Organisational units based on filter and list."""
    area_detail, result = {}, ()
    if add_select:
        area_detail[''] = 'Please Select'
    try:
        if geo_lists:
            for i, geo_list in enumerate(geo_lists):
                area_id = geo_list['area_id']
                area_name = geo_list['area_name']
                area_type = geo_list['area_type_id']
                if geo_filter == area_type:
                    if user_filter:
                        if area_id in user_filter:
                            area_detail[area_id] = area_name
                    else:
                        area_detail[area_id] = area_name
            result = area_detail.items()
    except Exception, e:
        raise e
    else:
        return result