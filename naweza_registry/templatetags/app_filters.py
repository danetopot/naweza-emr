from django import template

register = template.Library()


@register.filter(name='gen_value')
def gen_value(value, args):
	if value in args:
		return args[value]
	else:
		return value

@register.filter(name='gen_text')
def gen_value(value):
	if value=='dbp':
		value = 'Diastolic Blood Pressure'
	if value=='sbp':
		value = 'Systolic Blood Pressure'
	if value=='tobacco_use':
		value = 'Smoking'
	if value=='diabetes':
		value = 'Diabetic'
	if value=='blood_sugar':
		value = 'Blood Sugar Level'
		return value
		# return ['Diastolic Blood Pressure']
	else:
		return value
