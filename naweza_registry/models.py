from django.db import models
from django.utils import timezone
from naweza_auth.models import AppUser
from naweza_main.models import SetupGeography
import uuid

# Create your models here.
class RegOrgUnit(models.Model):
	"""Model for Organisational Units details."""
	org_unit = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	org_unit_name = models.CharField(max_length=255, null=False)
	org_unit_type = models.CharField(max_length=20, default='TNCL')
	org_unit_contacts = models.CharField(max_length=20, null=True)
	org_unit_postal_address = models.CharField(max_length=20, null=True)
	org_unit_area = models.ForeignKey(SetupGeography)
	date_operational = models.DateField(null=True, blank=True)
	date_closed = models.DateField(null=True, blank=True)
	is_void = models.BooleanField(default=False)
	created_by = models.ForeignKey(AppUser, null=True)
	created_at = models.DateField(default=timezone.now)
	sync_id = models.UUIDField(default=uuid.uuid1, editable=False)
	rec_status = models.CharField(max_length=2, default='I', choices=[('I', 'Inserted'), ('D', 'Downloaded')])

	def _is_active(self):
		if self.date_closed:
			return False
		else:
			return True

	is_active = property(_is_active)

	class Meta:
		"""Override table details."""

		db_table = 'reg_org_unit'
		verbose_name = 'Organisational Units Registry'
		verbose_name_plural = 'Organisational Units Registries'

	def make_void(self, date_closed=None):
		"""Inline call method."""
		self.is_void = True
		if date_closed:
			self.date_closed = date_closed
		super(RegOrgUnit, self).save()

	def __unicode__(self):
		"""To be returned by admin actions."""
		return self.org_unit_name

class RegPerson(models.Model):
	"""Model for Persons details."""
	person = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	designation = models.CharField(max_length=25, null=True)
	first_name = models.CharField(max_length=255, null=True)
	other_names = models.CharField(max_length=255, default=None, null=True)
	surname = models.CharField(max_length=255, default=None, null=True)
	email = models.EmailField(blank=True, default=None, null=True)
	des_phone_number = models.IntegerField(null=True, blank=True, default=None)
	postal_address = models.CharField(max_length=100, null=True)
	date_of_birth = models.DateField(null=True)
	date_of_death = models.DateField(null=True, blank=True, default=None)
	# age = models.IntegerField(default=None, null=True)	
	sex_id = models.CharField(max_length=4, null=True, choices=[('SMAL', 'Male'), ('SFEM', 'Female')])
	# clinic = models.ForeignKey(RegOrgUnit, default=1)
	is_void = models.BooleanField(default=False)
	created_by = models.ForeignKey(AppUser, null=True)
	created_at = models.DateField(default=timezone.now)
	sync_id = models.UUIDField(default=uuid.uuid1, editable=False)
	rec_status = models.CharField(max_length=2, default='I', choices=[('I', 'Inserted'), ('D', 'Downloaded')])

	def _get_persons_data(self):
		_reg_persons_data = RegPerson.objects.all().order_by('-id')
		return _reg_persons_data

	def _get_full_name(self):
		return '%s %s %s' % (self.first_name, self.other_names, self.surname)

	def make_void(self):
		"""Inline call method."""
		self.is_void = True
		super(RegPerson, self).save()

	class Meta:
		"""Override table details."""

		db_table = 'reg_person'
		verbose_name = 'Persons Registry'
		verbose_name_plural = 'Persons Registries'

	def __unicode__(self):
		"""To be returned by admin actions."""
		return '%s %s %s' % (self.first_name, self.other_names, self.surname)


class RegPersonsGeo(models.Model):
	"""Model for Persons Geography."""
	person = models.ForeignKey(RegPerson)
	area = models.ForeignKey(SetupGeography)
	area_type = models.CharField(max_length=4)
	date_linked = models.DateField(null=True)
	date_delinked = models.DateField(null=True)
	is_void = models.BooleanField(default=False)

	def make_void(self, date_delinked, is_void):
		"""Inline call method."""
		if date_delinked:
			self.date_delinked = date_delinked
			self.is_void = True
		super(RegPersonsGeo, self).save()

	class Meta:
		"""Override table details."""

		db_table = 'reg_persons_geo'


class RegPersonsOrgUnits(models.Model):
	"""Model for Persons Organisational Units."""
	person = models.ForeignKey(RegPerson)
	org_unit = models.ForeignKey(RegOrgUnit)
	date_linked = models.DateField(null=True)
	date_delinked = models.DateField(null=True)
	# clinic = models.ForeignKey(RegOrgUnit, related_name='fk_clinic')
	is_void = models.BooleanField(default=False)
	created_by = models.ForeignKey(AppUser, null=True)
	created_at = models.DateField(default=timezone.now)
	sync_id = models.UUIDField(default=uuid.uuid1, editable=False)
	rec_status = models.CharField(max_length=2, default='I', choices=[('I', 'Inserted'), ('D', 'Downloaded')])

	class Meta:
		"""Override table details."""

		db_table = 'reg_persons_org_units'

# Baseline Table (Enrollment)
class AfyaData(models.Model):
	fname = models.CharField(null=False, max_length=15)
	lname = models.CharField(null=False, max_length=50)
	age = models.PositiveIntegerField(null=False) 
	sex = models.CharField(null=False, max_length=15) 
	glucose = models.DecimalField(null=False, decimal_places=2, max_digits=6, default=0.00)
	smoker = models.CharField(null=False, max_length=15)
	diabetes = models.CharField(null=False, max_length=15)
	phone_no = models.CharField(null=True, max_length=15)
	sbp = models.PositiveIntegerField(null=False)
	color = models.CharField(null=False, max_length=15)
	datecreated = models.DateTimeField(null=False)
	reporter = models.CharField(null=True, max_length=20)
	active = models.BooleanField(default=True)

	class Meta:
		db_table = 'afyachat_afyadata'

#class AfyaDataPatientIdentifier(models.Model):
#	class Meta:
#		db_table = 'afyachat_extra'

class AfyaDataPersonInfo(models.Model):
	info_type = models.CharField(max_length=20, null=False, default='INFO_TYPE')
	info_value = models.CharField(max_length=1000, null=False, default='INFO_VALUE')	
	patient = models.ForeignKey(AfyaData, null=True)
	is_void = models.BooleanField(default=False)
	created_by = models.ForeignKey(AppUser, null=True)
	created_at = models.DateField(default=timezone.now)
	sync_id = models.UUIDField(default=uuid.uuid1, editable=False)
	rec_status = models.CharField(max_length=2, default='I', choices=[('I', 'Inserted'), ('D', 'Downloaded')])

	class Meta:
		db_table = 'afyachat_personinfo'