# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings
import uuid


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('naweza_main', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AfyaData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fname', models.CharField(max_length=15)),
                ('lname', models.CharField(max_length=50)),
                ('age', models.PositiveIntegerField()),
                ('sex', models.CharField(max_length=15)),
                ('glucose', models.DecimalField(default=0.0, max_digits=6, decimal_places=2)),
                ('smoker', models.CharField(max_length=15)),
                ('diabetes', models.CharField(max_length=15)),
                ('phone_no', models.CharField(max_length=15, null=True)),
                ('sbp', models.PositiveIntegerField()),
                ('color', models.CharField(max_length=15)),
                ('datecreated', models.DateTimeField()),
                ('reporter', models.CharField(max_length=20, null=True)),
                ('active', models.BooleanField(default=True)),
            ],
            options={
                'db_table': 'afyachat_afyadata',
            },
        ),
        migrations.CreateModel(
            name='AfyaDataPersonInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('upn', models.CharField(max_length=15, null=True)),
                ('id_number', models.CharField(max_length=10, null=True)),
                ('income', models.IntegerField(default=None, null=True)),
                ('is_void', models.BooleanField(default=False)),
                ('created_at', models.DateField(default=django.utils.timezone.now)),
                ('sync_id', models.UUIDField(default=uuid.uuid1, editable=False)),
                ('rec_status', models.CharField(default=b'I', max_length=2, choices=[(b'I', b'Inserted'), (b'D', b'Downloaded')])),
                ('afyadata_id', models.ForeignKey(to='naweza_registry.AfyaData', null=True)),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'db_table': 'afyachat_personinfo',
            },
        ),
        migrations.CreateModel(
            name='RegOrgUnit',
            fields=[
                ('org_unit', models.UUIDField(default=uuid.uuid1, serialize=False, editable=False, primary_key=True)),
                ('org_unit_name', models.CharField(max_length=255)),
                ('org_unit_type', models.CharField(default=b'TNCL', max_length=20)),
                ('org_unit_contacts', models.CharField(max_length=20, null=True)),
                ('org_unit_postal_address', models.CharField(max_length=20, null=True)),
                ('date_operational', models.DateField(null=True, blank=True)),
                ('date_closed', models.DateField(null=True, blank=True)),
                ('is_void', models.BooleanField(default=False)),
                ('created_at', models.DateField(default=django.utils.timezone.now)),
                ('sync_id', models.UUIDField(default=uuid.uuid1, editable=False)),
                ('rec_status', models.CharField(default=b'I', max_length=2, choices=[(b'I', b'Inserted'), (b'D', b'Downloaded')])),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
                ('org_unit_area', models.ForeignKey(to='naweza_main.SetupGeography')),
            ],
            options={
                'db_table': 'reg_org_unit',
                'verbose_name': 'Organisational Units Registry',
                'verbose_name_plural': 'Organisational Units Registries',
            },
        ),
        migrations.CreateModel(
            name='RegPerson',
            fields=[
                ('person', models.UUIDField(default=uuid.uuid1, serialize=False, editable=False, primary_key=True)),
                ('designation', models.CharField(max_length=25, null=True)),
                ('first_name', models.CharField(max_length=255, null=True)),
                ('other_names', models.CharField(default=None, max_length=255, null=True)),
                ('surname', models.CharField(default=None, max_length=255, null=True)),
                ('email', models.EmailField(default=None, max_length=254, null=True, blank=True)),
                ('des_phone_number', models.IntegerField(default=None, null=True, blank=True)),
                ('postal_address', models.CharField(max_length=100, null=True)),
                ('date_of_birth', models.DateField(null=True)),
                ('date_of_death', models.DateField(default=None, null=True, blank=True)),
                ('sex_id', models.CharField(max_length=4, null=True, choices=[(b'SMAL', b'Male'), (b'SFEM', b'Female')])),
                ('is_void', models.BooleanField(default=False)),
                ('created_at', models.DateField(default=django.utils.timezone.now)),
                ('sync_id', models.UUIDField(default=uuid.uuid1, editable=False)),
                ('rec_status', models.CharField(default=b'I', max_length=2, choices=[(b'I', b'Inserted'), (b'D', b'Downloaded')])),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'db_table': 'reg_person',
                'verbose_name': 'Persons Registry',
                'verbose_name_plural': 'Persons Registries',
            },
        ),
        migrations.CreateModel(
            name='RegPersonsGeo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('area_type', models.CharField(max_length=4)),
                ('date_linked', models.DateField(null=True)),
                ('date_delinked', models.DateField(null=True)),
                ('is_void', models.BooleanField(default=False)),
                ('area', models.ForeignKey(to='naweza_main.SetupGeography')),
                ('person', models.ForeignKey(to='naweza_registry.RegPerson')),
            ],
            options={
                'db_table': 'reg_persons_geo',
            },
        ),
        migrations.CreateModel(
            name='RegPersonsOrgUnits',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_linked', models.DateField(null=True)),
                ('date_delinked', models.DateField(null=True)),
                ('is_void', models.BooleanField(default=False)),
                ('created_at', models.DateField(default=django.utils.timezone.now)),
                ('sync_id', models.UUIDField(default=uuid.uuid1, editable=False)),
                ('rec_status', models.CharField(default=b'I', max_length=2, choices=[(b'I', b'Inserted'), (b'D', b'Downloaded')])),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
                ('org_unit', models.ForeignKey(to='naweza_registry.RegOrgUnit')),
                ('person', models.ForeignKey(to='naweza_registry.RegPerson')),
            ],
            options={
                'db_table': 'reg_persons_org_units',
            },
        ),
    ]
