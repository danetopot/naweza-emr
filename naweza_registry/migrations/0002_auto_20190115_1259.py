# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('naweza_registry', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='afyadatapersoninfo',
            old_name='afyadata_id',
            new_name='patient',
        ),
        migrations.RemoveField(
            model_name='afyadatapersoninfo',
            name='id_number',
        ),
        migrations.RemoveField(
            model_name='afyadatapersoninfo',
            name='income',
        ),
        migrations.RemoveField(
            model_name='afyadatapersoninfo',
            name='upn',
        ),
        migrations.AddField(
            model_name='afyadatapersoninfo',
            name='info_type',
            field=models.CharField(default=b'INFO_TYPE', max_length=20),
        ),
        migrations.AddField(
            model_name='afyadatapersoninfo',
            name='info_value',
            field=models.CharField(default=b'INFO_VALUE', max_length=1000),
        ),
    ]
