from datetime import date
from django.shortcuts import render, get_object_or_404
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.utils import timezone
from naweza_main.functions import get_list_of_persons, get_list_of_orgs, get_dict
from naweza_registry.forms import SearchForm, PersonRegistrationForm, ClinicRegistrationForm, ExitForm
from naweza_registry.models import RegPersonsOrgUnits, RegOrgUnit, RegPerson, RegPersonsGeo, AfyaData, AfyaDataPersonInfo
from naweza_auth.models import AppUser
from naweza_main.models import SetupGeography
from naweza_forms.models import PatientVisits
from naweza_main.functions import convert_date
from naweza.functions import generate_color, generate_age

# Create your views here.
def visits_search(request):
	check_fields = ['sex_id']
	vals = get_dict(field_name=check_fields)
	resultsets = None
	try:
		search_id = request.GET.get('id')
		init_data = AfyaData.objects.get(pk=search_id, active=True)
		resultsets = PatientVisits.objects.filter(patient_id=search_id).order_by('-visit_date')
		for resultset in resultsets:
			resultset.data = init_data

		return render(request, 'forms/visits.html',
					  {'status': 200,
					  'resultsets': resultsets,
					  'init_data': init_data,
					  'vals': vals})
	except Exception, e:
		raise e

def person_search(request):
	"""Some default page for Bad request error page."""
	form = SearchForm(data=request.POST)
	check_fields = ['sex_id']
	vals = get_dict(field_name=check_fields)
	resultsets = None

	try:
		if request.method == 'POST':
			resultsets = None
			search_string = request.POST.get('search_name')
			search_criteria = request.POST.get('search_criteria')
			resultsets = get_list_of_persons(search_string=search_string, search_criteria=search_criteria)

			if not resultsets:
				msg = 'No results for (%s).Person does not exist in database.' % search_string
				messages.add_message(request, messages.ERROR, msg)
				return render(request, 'registry/persons_index.html', {'form': form})

			for resultset in resultsets:
				resultset.search_criteria = search_criteria

			msg = 'Showing  results for (%s)' % search_string
			messages.add_message(request, messages.INFO, msg)
			return render(request, 'registry/persons_index.html',
			{ 
				'status': 400, 'form': form, 'vals': vals,
				'resultsets': resultsets
			})
		else:
			search_id = request.GET.get('person')
			search_criteria = 'PTPT'
			if search_id and search_id.isdigit():
				resultsets = get_list_of_persons(search_string=search_id, search_criteria=search_criteria)
				for resultset in resultsets:
					resultset.search_criteria = search_criteria

				return render(request, 'registry/persons_index.html',
				{ 
					'status': 400, 'form': form, 'vals': vals,
					'resultsets': resultsets
				})
				
	except Exception, e:
		raise e
	return render(request, 'registry/persons_index.html', { 'form': form })

def new_person(request):
	"""Some default page for Bad request error page."""
	try:
		if request.method == 'POST':
			now = timezone.now()
			form = PersonRegistrationForm(data=request.POST)

			# person_type = request.POST.get('person_type') if request.POST.get('person_type') else None
			# upn = request.POST.get('upn') if request.POST.get('id_number') else None
			first_name = (request.POST.get('first_name')).upper() if request.POST.get('first_name') else None
			other_names = (request.POST.get('other_names')).upper() if request.POST.get('other_names') else None
			surname = (request.POST.get('surname')).upper() if request.POST.get('surname') else None
			sex_id = request.POST.get('sex_id') if request.POST.get('sex_id') else None
			date_of_birth = request.POST.get('date_of_birth') if request.POST.get('date_of_birth') else None
			id_number = request.POST.get('id_number') if request.POST.get('id_number') else None
			income = request.POST.get('income') if request.POST.get('income') else None
			sbp = request.POST.get('sbp') if request.POST.get('sbp') else None
			glucose = request.POST.get('glucose') if request.POST.get('glucose') else None
			smoker = request.POST.get('smoker') if request.POST.get('smoker') else None
			diabetes = request.POST.get('diabetes') if request.POST.get('diabetes') else None
			des_phone_number = request.POST.get('des_phone_number') if request.POST.get('des_phone_number') else None
			# email = request.POST.get('email') if request.POST.get('email') else None
			# postal_address = (request.POST.get('postal_address')).upper() if request.POST.get('postal_address') else None
			# age = request.POST.get('age')		
			# living_in = request.POST.get('living_in') if request.POST.get('living_in') else None
			# clinic = request.POST.get('clinic') if request.POST.get('clinic') else None

			age = generate_age(date_of_birth)
			color = generate_color(age, sex_id, smoker, diabetes, sbp)
			lname = (other_names + ' ' + surname) if surname else other_names
			print 'color : %s' %color
			# if person_type == 'PTUS':
			afyachat = AfyaData(
				fname = first_name,
				lname = lname,
				age = generate_age(date_of_birth),
				sex = sex_id, 
				glucose = glucose,
				smoker = smoker,
				diabetes = diabetes,
				phone_no = des_phone_number,
				sbp = sbp,
				color = color,
				reporter = 'EMR (via WEB-UI)',
				datecreated = date.today()				
				)
			afyachat.save()
			afyachat_pk=afyachat.pk			

			AfyaDataPersonInfo(
				# reg_person = RegPerson.objects.get(pk=str(reg_person_pk)),
				afyadata_id = AfyaData.objects.get(pk=str(afyachat_pk)),
				upn = None,
				id_number = id_number,	
				income = income
				).save()

			msg = 'Person Save Success'
			messages.add_message(request, messages.INFO, msg)
			return HttpResponseRedirect('%s?person=%s' % (reverse(person_search), str(afyachat_pk)))		

		form = PersonRegistrationForm()		
		return render(request, 'registry/person_new.html', {'status': 400, 'form': form })
	except Exception, e:
		msg = 'Person Save Error - %s' %str(e)
		messages.add_message(request, messages.ERROR, msg)
		redirect_url = reverse(person_search)
		return HttpResponseRedirect(redirect_url)
		raise e
	

def edit_person(request):
	"""Some default page for Bad request error page."""
	check_fields = ['sex_id']
	vals = get_dict(field_name=check_fields)
	try:
		return render(request, 'registry/person_edit.html', {'vals': vals})
	except Exception, e:
		raise e

def edit_patient(request, id):
	"""Some default page for Bad request error page."""
	check_fields = ['sex_id']
	vals = get_dict(field_name=check_fields)
	try:
		return render(request, 'registry/person_edit.html', {'vals': vals})
	except Exception, e:
		raise e

def view_person(request, uuid):
	"""Some default page for Bad request error page."""
	try:
		check_fields = ['sex_id']
		vals = get_dict(field_name=check_fields)
		person = RegPerson.objects.get(pk=uuid)
		appuser = AppUser.objects.get(reg_person=person)
		person_orgs = RegPersonsOrgUnits.objects.get(person=uuid)
		org_unit = RegOrgUnit.objects.get(org_unit=person_orgs.org_unit_id)
		org_unit_name = org_unit.org_unit_name


		return render(request, 'registry/person_view.html', 
			{'person': person, 'org_unit_name': org_unit_name, 'vals': vals,
			  'appuser': appuser})
	except Exception, e:
		raise e

def view_patient(request, id):
	"""Some default page for Bad request error page."""
	try:
		check_fields = ['sex_id']
		vals = get_dict(field_name=check_fields)
		person = AfyaData.objects.get(pk=id)
		personinfo = AfyaDataPersonInfo.objects.filter(patient_id=id)
		person.pinfo = personinfo

		idnumber = None
		income = None
		if personinfo:
			for pinfo in personinfo:
				idnumber = pinfo.id_number
				income = pinfo.income

		return render(request, 'registry/patient_view.html',
		 { 'person': person, 'vals': vals })
	except Exception, e:
		raise e

def delete_person(request, uuid):
	"""Some default page for Bad request error page."""
	try:
		return render(request, '400.html', {'status': 400})
	except Exception, e:
		raise e

def delete_patient(request, id):
	"""Some default page for Bad request error page."""
	try:
		return render(request, '400.html', {'status': 400})
	except Exception, e:
		raise e

def exit_patient(request, id):
	try:
		if request.method == 'POST':
			exit_reason = request.POST.get('exit_reason')
			exit_reason_other = request.POST.get('exit_reason_other')

			info_value = exit_reason_other if exit_reason_other else exit_reason
			AfyaDataPersonInfo(
				patient=AfyaData.objects.get(pk=id),
				info_type='EXIT_REASON',
				info_value=info_value
				).save()
			AfyaData.objects.filter(id=id).update(active=False)


			msg = 'Patient Exited Successfully'
			messages.add_message(request, messages.INFO, msg)
			form = SearchForm(data=request.POST)
			return render(request, 
				'registry/persons_index.html',
				 { 
					'status': 400, 
					'form': form,
				  })
		else:
			form = ExitForm()
			return render(request, 'registry/patient_exit.html', { 'form': form })
	except Exception, e:
		raise e
		"""
		msg = 'Error Exiting Patient :- %s' %str(e)
		messages.add_message(request, messages.ERROR, msg)
		return render(request, 'registry/patient_exit.html', { 'form': form })
		"""


def clinic_search(request):
	"""Some default page for Bad request error page."""
	form = SearchForm(data=request.POST)
	try:
		if request.method == 'POST':
			resultsets = None
			search_string = request.POST.get('search_name')

			resultsets = get_list_of_orgs(search_string=search_string)
			if not resultsets:
				msg = 'No results for (%s).Clinic does not exist in database.' % search_string
				messages.add_message(request, messages.ERROR, msg)
				return render(request, 'registry/clinics_index.html', {'form': form})

			for resultset in resultsets:
				# get count
				org_geo = SetupGeography.objects.get(area_id=resultset.org_unit_area_id) 
				resultset.area = org_geo.area_name

			msg = 'Showing  results for (%s)' % search_string
			messages.add_message(request, messages.INFO, msg)
			return render(request, 
				'registry/clinics_index.html',
				 { 
					'status': 400, 
					'form': form,
					'resultsets': resultsets
	  			})
	except Exception, e:
		raise e
	return render(request, 'registry/clinics_index.html', { 'form': form })

def new_clinic(request):
	"""Some default page for Bad request error page."""
	msg = ''
	try:
		if request.method == 'POST':
			form = ClinicRegistrationForm(data=request.POST)
			org_unit_name = request.POST.get('clinic_name')
			org_unit_contacts = request.POST.get('contacts')
			org_unit_postal_address = request.POST.get('postal_address')
			org_unit_area = request.POST.get('area_id')

			RegOrgUnit(
				org_unit_name = org_unit_name,
				org_unit_contacts = org_unit_contacts,
				org_unit_postal_address = org_unit_postal_address,
				org_unit_area = SetupGeography.objects.get(area_id=org_unit_area)
				).save()
			msg = 'Clinic Save Success'
			messages.add_message(request, messages.INFO, msg)
			redirect_url = reverse(clinic_search)
			return HttpResponseRedirect(redirect_url)
		else:
			form = ClinicRegistrationForm()
			return render(request, 'registry/clinic_new.html', {'status': 400, 'form': form })

	except Exception, e:
		msg = 'Clinic Save Error - %s' %str(e)
		messages.add_message(request, messages.ERROR, msg)
		redirect_url = reverse(clinic_search)
		return HttpResponseRedirect(redirect_url)
		raise e
	

def edit_clinic(request):
	"""Some default page for Bad request error page."""
	try:
		return render(request, '400.html', {'status': 400})
	except Exception, e:
		raise e

def view_clinic(request):
	"""Some default page for Bad request error page."""
	try:
		return render(request, '400.html', {'status': 400})
	except Exception, e:
		raise e

def delete_clinic(request):
	"""Some default page for Bad request error page."""
	try:
		return render(request, '400.html', {'status': 400})
	except Exception, e:
		raise e

