from django import forms
from django.utils.translation import ugettext_lazy as _
from naweza_main.functions import get_list, get_org_units_list
from .functions import (get_all_geo_list, get_geo_list)

all_list = get_all_geo_list()
county_list = [('', 'Please Select')] + list(get_geo_list(all_list, 'GPRV'))
sub_county_list = get_geo_list(all_list, 'GDIS')
ward_list = get_geo_list(all_list, 'GWRD')
exit_reason_list = get_list('exit_reason_id', 'Please Select')
sex_id_list = get_list('sex_id', 'Please Select')
clinic_list = get_list('health_facility_id', 'Please Select')
psearch_filter_list = get_list('psearch_filter_id', 'Select Criteria')
ptype_list = get_list('psearch_filter_id', 'Please Select')
yesno_list = get_list('yes_no_id', 'Please Select')

class ExitForm(forms.Form): 
    exit_reason = forms.ChoiceField(choices=exit_reason_list,
        initial='0',
        widget=forms.Select(
          attrs={'class': 'form-control',
          'id': 'exit_reason',
          'data-parsley-required': 'true'
        }))
    exit_reason_other = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Specify(Other)'),
               'class': 'form-control',
               'id': 'exit_reason_other',
               'data-parsley-required': 'true'}))

class SearchForm(forms.Form):
    """Search registry form."""
    search_name = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Enter Name . . .'),
               'class': 'form-control',
               'id': 'search_name',
               'data-parsley-group': 'primary',
               'data-parsley-required': 'true'}))
    search_criteria = forms.ChoiceField(
        choices=psearch_filter_list,
        initial='0',
        required=True,
        widget=forms.Select(
            attrs={'class': 'form-control',
                   'id': 'search_criteria',
                   'data-parsley-required': 'true'}))

class PersonRegistrationForm(forms.Form):
  # Tab 1
    first_name = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('First Name'),
               'class': 'form-control',
               'id': 'first_name',
               'data-parsley-required': "true",
               'data-parsley-group': 'group0'}))
    other_names = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Other Names'),
               'class': 'form-control',
               'id': 'other_names',
               'data-parsley-required': "true",
               'data-parsley-group': 'group0'}))
    surname = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Surname'),
               'class': 'form-control',
               'id': 'surname'}))
    sex_id = forms.ChoiceField(
        choices=sex_id_list,
        widget=forms.Select(
            attrs={'placeholder': _('Sex'),
                   'class': 'form-control',
                   'id': 'sex_id',
                   'data-parsley-required': "true",
                   'data-parsley-group': 'group0'}))
    date_of_birth = forms.DateField(widget=forms.DateInput(
        attrs={'placeholder': _('Date Of Birth'),
               'class': 'form-control',
               'id': 'date_of_birth',
               'data-parsley-required': "true",
               'data-parsley-group': 'group0'
               }))
    age = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Age (Years)'),
               'class': 'form-control',
               'id': 'age',
               'data-parsley-required': "true",
               # 'type': "number",
               'data-parsley-type': "digits",
               'data-parsley-group': 'group0'}))
    id_number = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('ID Number'),
               'class': 'form-control',
               'id': 'id_number',
               'data-parsley-required': "true",
               'data-parsley-type': "digits",
               'data-parsley-group': 'group0'}))
    upn = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('UPN'),
               'class': 'form-control',
               'id': 'upn',
               'data-parsley-required': "true",
               'data-parsley-group': 'group0'}))
    person_type = forms.ChoiceField(
        choices=ptype_list,
        initial='',
        widget=forms.Select(
            attrs={'placeholder': _('Select'),
                   'class': 'form-control',
                   'id': 'person_type',
                   'data-parsley-required': "true",
                   'data-parsley-group': 'group1'}))

    # Tab 2
    income = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Income (pm)'),
               'class': 'form-control',
               'id': 'income'}))
    smoker = forms.ChoiceField(
        choices=yesno_list,
        widget=forms.Select(
            attrs={'placeholder': _('Select'),
                   'class': 'form-control',
                   'id': 'smoker',
                   'data-parsley-required': "true",
                   'data-parsley-group': 'group1'}))
    diabetes = forms.ChoiceField(
        choices=yesno_list,
        initial='',
        widget=forms.Select(
            attrs={'placeholder': _('Select'),
                   'class': 'form-control',
                   'id': 'diabetes',
                   'data-parsley-required': "true",
                   'data-parsley-group': 'group1'}))
    sbp = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('SBP'),
               'class': 'form-control',
               'id': 'sbp',
               'data-parsley-required': "true",
               'data-parsley-type': "digits",
               'data-parsley-group': 'group1'}))
    glucose = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Gluose(mmol)'),
               'class': 'form-control',
               'id': 'glucose',
               'data-parsley-required': "true",
               'data-parsley-type': "number",
               'data-parsley-group': 'group1'}))

    # Tab 3
    phone_number = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Phone Number'),
               'class': 'form-control',
               'id': 'phone_number',
               'data-parsley-required': "true",
               'data-parsley-group': 'group2'}))
    email = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Email'),
               'class': 'form-control',
               'id': 'email',
               'data-parsley-type': "email",
               'data-parsley-group': 'group2'}))
    postal_address = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Postal Address'),
               'class': 'form-control',
               'id': 'postal_address',
               'data-parsley-group': 'group2'}))
    living_in = forms.ChoiceField(
        choices=county_list,
        initial='',
        widget=forms.Select(
            attrs={'placeholder': _('Sex'),
                   'class': 'form-control',
                   'id': 'living_in',
                   'data-parsley-required': "true",
                   'data-parsley-group': 'group2'}))
    """
    clinic = forms.ChoiceField(
        choices=clinic_list,
        widget=forms.Select(
            attrs={'placeholder': _('Sex'),
                   'class': 'form-control',
                   'id': 'clinic',
                   'data-parsley-required': "true",
                   'data-parsley-group': 'group2'}))
    """
    def __init__(self, *args, **kwargs):
        super(PersonRegistrationForm, self).__init__(*args, **kwargs)
        org_units_list = get_org_units_list('Please Select Clinic')

        clinic = forms.ChoiceField(
            choices=org_units_list, label=_('Select clinic'),
            initial='0',
            widget=forms.Select(
                attrs={'id': 'clinic',
                       'class': 'form-control',
                       'data-parsley-required': "true",
                       'data-parsley-group': "group2"}))
        self.fields['clinic'] = clinic

class ClinicRegistrationForm(forms.Form):
    clinic_name = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Clinic Name'),
               'class': 'form-control',
               'id': 'clinic_name',
               'data-parsley-required': "true"}))
    postal_address = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Postal Address'),
               'class': 'form-control',
               'id': 'postal_address'}))
    contacts = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Contacts'),
               'class': 'form-control',
               'id': 'contacts',
               'data-parsley-required': "true"}))
    area_id = forms.ChoiceField(
        choices=county_list,
        widget=forms.Select(
            attrs={'placeholder': _('County'),
                   'class': 'form-control',
                   'id': 'area_id',
                   'data-parsley-required': "true"}))
